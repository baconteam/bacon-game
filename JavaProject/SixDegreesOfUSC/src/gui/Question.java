package gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import client.Constants;

public class Question extends Panel implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private JLabel m_questionTitle, answerLabel, promptLabel;
	private ButtonGroup m_buttonGroup;
	private ArrayList<RadioButton> m_possibleResponses;
	private Button m_submitButton, m_requestNewQuestionButton;
	private JPanel questionPanel, buttonPanel;
	private ArrayList<JLabel> m_classes;
	
	private Vector<String> m_question;
	private String[] m_students;
	private String m_answer;
	
	private int GridY;
	
	private final String m_questionPrompt = "Identify the user with the following schedule:";
	
	private final int GET_NEW_QUESTION = 0;
	private final int SUBMIT_QUESTION = 1;
		
	{
		m_possibleResponses = new ArrayList<RadioButton>();
		m_classes = new ArrayList<JLabel>();
		m_buttonGroup = new ButtonGroup();
	}
	
	public Question(Vector<String> question, Vector<String> students, String answer, ActionListener[] actionListeners)
	{
		super(actionListeners, null);

		m_question = question;
		m_students = new String[students.size()];
		for(int i = 0; i < students.size(); ++i)
		{
			m_students[i] = students.get(i);
		}
		m_answer = answer;
		PostInitialize();
		PostCreateGUI();
		PostAddEvents();
	}
	
	public String GetCorrectAnswer() { return m_answer; }
	
	private void PostInitialize()
	{
		for(int i = 0; i < m_question.size(); ++i)
		{
			JLabel label = new JLabel((i + 1) + ". " + m_question.get(i));
			label.setFont(client.Constants.BaseFont);
			label.setForeground(client.Constants.blue);
			m_classes.add(label);
		}
		// m_questionTitle = new JLabel(m_question);
		// m_questionTitle.setFont(client.Constants.QuestionFont);
		for(int i = 0; i < m_students.length; ++i)
		{
			RadioButton radioButton = new RadioButton(m_students[i], Constants.Header4Size);
			m_possibleResponses.add(radioButton);
		}
		Random rand = new Random();
		int index = rand.nextInt(4);
		RadioButton radioButton = new RadioButton(m_answer, Constants.Header4Size);
		m_possibleResponses.add(index, radioButton);
		for(int i = 0; i < m_possibleResponses.size(); ++i)
		{
			m_buttonGroup.add( m_possibleResponses.get(i).GetButton());
		}
		
		m_submitButton = new Button("Submit", Constants.Header2Size);
		m_requestNewQuestionButton = new Button("New Question", Constants.Header2Size);
		questionPanel = new JPanel();
		questionPanel.setLayout(new GridBagLayout());
		buttonPanel = new JPanel(new GridBagLayout());
		answerLabel = new JLabel();
		answerLabel.setFont(client.Constants.QuestionFont);
		promptLabel = new JLabel(m_questionPrompt);
		promptLabel.setFont(client.Constants.QuestionFont);
	}
	
	private void PostCreateGUI()
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JPanel promptPanel = new JPanel();
		promptPanel.add(promptLabel);
		promptPanel.setBorder(new EmptyBorder(0, 30, 0, 30));
		add(promptPanel);
		JPanel classPanel = new JPanel();
		for(int i = 0; i < m_classes.size(); ++i)
		{
			classPanel.add(m_classes.get(i));
		}
		classPanel.setBorder(new EmptyBorder(0, 20, 0, 20));
		add(classPanel);
		//JPanel titlePanel = new JPanel();
		//titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));

		// titlePanel.add(m_questionTitle);
		// titlePanel.setBorder(new EmptyBorder(0, 30, 0, 30));
		// add(titlePanel);
		GridBagConstraints q = new GridBagConstraints();
		GridY = 0;
		q.insets = new Insets(0, 0, 0, 0);
		q.gridy = GridY;
		q.gridwidth = GridBagConstraints.HORIZONTAL;
		for(int i = 0; i < m_possibleResponses.size(); ++i)
		{
			questionPanel.add(m_possibleResponses.get(i), q);
			GridY++;
			q.gridy = GridY;
		}	
		questionPanel.setBorder(new EmptyBorder(0, 20, 0, 20));
		add(questionPanel);
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.ipadx = 70;
		gridBagConstraints.ipady = 20;
		gridBagConstraints.insets = new Insets(10, 20, 10, 20);
		buttonPanel.add(m_submitButton, gridBagConstraints);
		gridBagConstraints.gridy = 1;
		buttonPanel.add(m_requestNewQuestionButton, gridBagConstraints);
		add(buttonPanel);
		JPanel answerPanel = new JPanel();
		answerPanel.add(answerLabel);
		answerPanel.setBorder(new EmptyBorder(20,30,20,30));
		add(answerPanel);
		setBorder(new EmptyBorder(50, 50, 50, 50));
	}
	
	private void PostAddEvents()
	{
		m_submitButton.addActionListener(GetActionListener(SUBMIT_QUESTION));
		m_requestNewQuestionButton.addActionListener(GetActionListener(GET_NEW_QUESTION));
	}
	
	@Override
	public void InitializeComponents() 
	{
		
	}

	@Override
	public void CreateGUI() 
	{
		
	}

	@Override
	public void AddEvents() 
	{
		
		
//		m_submitButton.addActionListener(new ActionListener()
//		{
//			@Override
//			public void actionPerformed(ActionEvent actionEvent)
//			{
//				for(RadioButton button : m_possibleResponses)
//				{
//					if(button.GetButton().isSelected() && button.GetLabel().equals(m_answer))
//					{
//						Debug.DebugLog("Correct Answer");
//					}
//				}
//			}
//		});
	}
	
	public boolean IsCorrectAnswerSelected()
	{
		for(RadioButton button : m_possibleResponses)
		{
			if(button.GetButton().isSelected() && button.GetLabel().equals(m_answer))
			{
				return true;
			}
		}
		return false;
	}
	
	public void ClearLabel() {
		answerLabel.setText("");
	}
	
	public void Wrong() {
		answerLabel.setText("Wrong");
		answerLabel.setForeground(Color.red);
	}
}
