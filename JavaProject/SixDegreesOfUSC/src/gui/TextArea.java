package gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import client.Constants;

public class TextArea extends JTextArea implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private int m_fontSize;
	
	public TextArea(int fontSize)
	{
		super(4, 20);
		
		m_fontSize = fontSize;
		
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}
	
	public void AddText(String text)
	{
		setText(getText() + System.lineSeparator() + text);
	}
	
	@Override
	public void InitializeComponents() 
	{

	}

	@Override
	public void CreateGUI() 
	{
		setFont(Constants.BaseFont.deriveFont(Font.PLAIN, m_fontSize));
		setBackground(client.Constants.blue);
		setForeground(Color.white);
		setBorder(new LineBorder(client.Constants.orange, 2));
		setLineWrap(true);
		setWrapStyleWord(true);
		setOpaque(true);
		setEditable(false);
	}

	@Override
	public void AddEvents() 
	{
		
	}
}
