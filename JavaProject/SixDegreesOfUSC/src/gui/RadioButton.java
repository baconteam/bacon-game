package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import client.Constants;
import util.Debug;

public class RadioButton extends JPanel implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private JRadioButton m_radioButton;
	private String m_label;
	private int m_fontSize;
	
	public RadioButton(String label, int fontSize)
	{		
		super();
		
		m_label = label;

		InitializeComponents();
		CreateGUI();
		AddEvents();
	}
	
	public AbstractButton GetButton() { return m_radioButton; }
	public String GetLabel() { return m_radioButton.getText(); }
	
	@Override
	public void InitializeComponents()
	{
		m_radioButton = new JRadioButton(m_label);
	}
	
	@Override
	public void CreateGUI()
	{
		m_radioButton.setFont(Constants.QuestionFont);
		add(m_radioButton);
	}
	
	@Override
	public void AddEvents()
	{
		m_radioButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent actionEvent)
			{
				Debug.DebugLog(GetLabel());
			}
		});
	}
}
