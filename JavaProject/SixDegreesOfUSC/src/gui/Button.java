package gui;

import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

import client.Constants;
import util.ImageLibrary;

public class Button extends JButton implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private String m_label;
	private int m_fontSize;
	
	public Button(String label, int fontSize)
	{
		super(label);
		
		m_label = label;
		m_fontSize = fontSize;
		
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}
	
	public Button(String label, int fontSize, String selectedImage, String unselectedImage)
	{
		super(label);
		
		m_label = label;
		m_fontSize = fontSize;
		
		LoadImages(selectedImage, unselectedImage);
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}
	
	public String GetButtonLabel() { return m_label; }
	
	@Override
	public void InitializeComponents()
	{	
		
	}
	
	@Override
	public void CreateGUI()
	{
		setFont(Constants.BaseFont.deriveFont(Font.PLAIN, m_fontSize));
		setHorizontalTextPosition(JButton.CENTER);
		setVerticalTextPosition(JButton.CENTER);
		setOpaque(false);
        setBorder(new LineBorder(client.Constants.orange,2));
        setBackground(client.Constants.blue);
	}
	
	@Override
	public void AddEvents()
	{
		
	}
	
	public void LoadImages(String selectedImage, String unselectedImage)
	{
		setPressedIcon(ImageLibrary.GetImage(selectedImage));
		setIcon(ImageLibrary.GetImage(unselectedImage));
		setBorder(BorderFactory.createEmptyBorder());
	}
}
