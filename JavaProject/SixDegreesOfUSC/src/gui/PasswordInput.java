package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.LineBorder;

import client.Constants;
import util.Debug;

public class PasswordInput extends JPanel implements IGUI
{
private static final long serialVersionUID = 1L;
	
	private JLabel m_inputPromptLabel;
	private JPasswordField m_passwordTextField;
	private String m_prompt;
	private GUILayout m_layout;
	private int m_fontSize;
	private int m_textFieldSize;
	
	
	public PasswordInput(String prompt, GUILayout layout, int fontSize, int textFieldSize)
	{
		super();
		
		m_prompt = prompt;
		m_layout = layout;
		m_fontSize = fontSize;
		m_textFieldSize = textFieldSize;
		
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}

	public void RemoveText(){ m_passwordTextField.setText(null);}
	public String GetPrompt() { return m_inputPromptLabel.getText(); }
	public String GetPassword() { return new String(m_passwordTextField.getPassword()); }
	
	@Override
	public void InitializeComponents()
	{
		m_inputPromptLabel = new JLabel(m_prompt);
		m_inputPromptLabel.setFont(Constants.BaseFont.deriveFont(Font.PLAIN, m_fontSize));
		m_inputPromptLabel.setForeground(client.Constants.blue);
		
		m_passwordTextField = new JPasswordField(m_textFieldSize);
		m_passwordTextField.setFont(Constants.BaseFont.deriveFont(Font.PLAIN, m_fontSize));
		m_passwordTextField.setBackground(client.Constants.blue);
		m_passwordTextField.setForeground(Color.white);
		m_passwordTextField.setBorder(new LineBorder(client.Constants.orange, 2));
		m_passwordTextField.setOpaque(true);
	}
	
	@Override
	public void CreateGUI()
	{	
		setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		switch(m_layout)
		{
			case Vertical:
				gridBagConstraints.gridy = 0;
				gridBagConstraints.anchor = GridBagConstraints.WEST;
				add(m_inputPromptLabel, gridBagConstraints);
				gridBagConstraints.gridy = 1;
				gridBagConstraints.weightx = 1;
				gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
				add(m_passwordTextField, gridBagConstraints);
				break;
			case Horizontal:
				gridBagConstraints.gridx = 0;
				add(m_inputPromptLabel, gridBagConstraints);
				gridBagConstraints.gridx = 1;
				gridBagConstraints.weightx = 1;
				gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
				add(m_passwordTextField, gridBagConstraints);
				break;
			default:
				Debug.DebugLog("Unknown layout format");
		}
		
		setOpaque(false);
	}
	
	@Override
	public void AddEvents()
	{
		
	}
}
