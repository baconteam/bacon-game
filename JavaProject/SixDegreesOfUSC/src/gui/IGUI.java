package gui;

public interface IGUI 
{
	void InitializeComponents();
	void CreateGUI();
	void AddEvents();
}
