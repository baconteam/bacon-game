package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Panel extends JPanel implements IGUI
{
    private static final long serialVersionUID = 1L;

    private ImageIcon m_image;
    private ActionListener[] m_actionListeners;
    protected boolean m_drawInBackground = false;
    protected Color m_backgroundColor = Color.WHITE;
    
    public Panel(ImageIcon inImage) 
    {
        m_image = inImage;
        
        InitializeComponents();
        CreateGUI();
        AddEvents();
    }
    
    public Panel(ActionListener[] actionListeners, ImageIcon inImage) 
    {
    	m_actionListeners = actionListeners;
        m_image = inImage;
        
        InitializeComponents();
        CreateGUI();
        AddEvents();
    }
    
    public Panel(ImageIcon inImage, boolean inDrawBack)
    {
        m_drawInBackground = inDrawBack;
        m_image = inImage;
        
        InitializeComponents();
        CreateGUI();
        AddEvents();
    }
    
    public Panel(ActionListener[] actionListeners, ImageIcon inImage, boolean inDrawBack) 
    {
    	m_actionListeners = actionListeners;
        m_drawInBackground = inDrawBack;
        m_image = inImage;
        
        InitializeComponents();
        CreateGUI();
        AddEvents();
    }
    
    public ActionListener GetActionListener(int index)
    {
    	if(index >= 0 && index < m_actionListeners.length)
    	{
    		return m_actionListeners[index];
    	}
    	else
    	{
    		return null;
    	}
    }
    
    @Override
    protected void paintComponent(Graphics graphics) 
    {
        super.paintComponent(graphics);
        
        if (m_image != null) {
            if(m_drawInBackground)
            {
                graphics.drawImage(m_image.getImage(), 0, 0, getWidth(), getHeight(), Color.WHITE, null);
            }
            else
            {
                graphics.drawImage(m_image.getImage(), 0, 0, getWidth(), getHeight(), null);
            }
        }

    }

	@Override
	public void InitializeComponents() 
	{
		setBackground(new Color(0, 0, 0, 0));
		setOpaque(true);
		repaint();
	}

	@Override
	public void CreateGUI() 
	{
		
	}

	@Override
	public void AddEvents() 
	{
		
	}
	
	public void SetImage(ImageIcon inImage) { m_image = inImage; }
}
