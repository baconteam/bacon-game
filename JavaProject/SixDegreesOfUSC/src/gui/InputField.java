package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import client.Constants;
import util.Debug;

public class InputField extends JPanel implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private JLabel m_inputPromptLabel;
	private JTextField m_inputTextField;
	private String m_prompt;
	private GUILayout m_layout;
	private int m_fontSize;
	private int m_textFieldSize;
	
	public InputField(String prompt, GUILayout layout, int fontSize, int textFieldSize)
	{
		super();
		
		m_prompt = prompt;
		m_layout = layout;
		m_fontSize = fontSize;
		m_textFieldSize = textFieldSize;
		
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}

	public String GetPrompt() { return m_prompt; }
	public String GetInput() { return m_inputTextField.getText(); }
	public void RemoveText() { m_inputTextField.setText(null);}
	@Override
	public void InitializeComponents()
	{
		m_inputPromptLabel = new JLabel(m_prompt);
		m_inputPromptLabel.setFont(Constants.BaseFont.deriveFont(Font.PLAIN, m_fontSize));
		m_inputPromptLabel.setForeground(client.Constants.blue);
		
		m_inputTextField = new JTextField(m_textFieldSize);
		m_inputTextField.setFont(Constants.BaseFont.deriveFont(Font.PLAIN, m_fontSize));
		m_inputTextField.setBackground(client.Constants.blue);
		m_inputTextField.setForeground(Color.white);
		m_inputTextField.setBorder(new LineBorder(client.Constants.orange, 2));
	}
	
	@Override
	public void CreateGUI()
	{	
		setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		switch(m_layout)
		{
			case Vertical:
				gridBagConstraints.gridy = 0;
				gridBagConstraints.anchor = GridBagConstraints.WEST;
				add(m_inputPromptLabel, gridBagConstraints);
				gridBagConstraints.gridy = 1;
				gridBagConstraints.weightx = 1;
				gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
				add(m_inputTextField, gridBagConstraints);
				break;
			case Horizontal:
				gridBagConstraints.gridx = 0;
				add(m_inputPromptLabel, gridBagConstraints);
				gridBagConstraints.gridx = 1;
				gridBagConstraints.weightx = 1;
				gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
				add(m_inputTextField, gridBagConstraints);
				break;
			default:
				Debug.DebugLog("Unknown layout format");
		}
		
		setOpaque(false);
	}
	
	@Override
	public void AddEvents()
	{
		
	}
	
	public void AddToolTip(String text) 
	{
		m_inputTextField.setToolTipText(text);
	}
}
