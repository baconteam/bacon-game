package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import server.Constants;

public class Table extends JPanel implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private JTable m_table;
	private JLabel m_tableTitleLabel;
	private JScrollPane m_scrollPane;
	private TableRowSorter<TableModel> m_tableSorter;
	private DefaultTableModel m_tableModel;
	private Object[] m_columnNames;
	private String m_tableTitle;
	private int m_titleFontSize, m_elementFontSize;
	
	public Table(String tableTitle, Object[] columnNames, int titleFontSize, int elementFontSize)
	{
		m_tableTitle = tableTitle;
		m_columnNames = columnNames;
		m_titleFontSize = titleFontSize;
		m_elementFontSize = elementFontSize;
		
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}
	
	public void AddElement(Object[] rowElement)
	{
		m_tableModel.addRow(rowElement);
		revalidate();
		repaint();
	}
	
	public void RemoveElement(Object key, int column)
	{
		for(int i = 0; i < m_tableModel.getRowCount(); ++i)
		{
			if(m_tableModel.getValueAt(i,  column).equals(key))
			{
				m_tableModel.removeRow(i);
				break;
			}
		}
		revalidate();
		repaint();
	}
	public boolean ElementExist(Object key, int column)
	{
		for(int i = 0; i < m_tableModel.getRowCount(); ++i)
		{
			if(m_tableModel.getValueAt(i,  column).equals(key))
			{
				return true;
			}
		}
		return false;
	}
	public int FindElementRow(Object key, int column)
	{
		for(int i = 0; i < m_tableModel.getRowCount(); ++i)
		{
			if(m_tableModel.getValueAt(i,  column).equals(key))
			{
				return i;
			}
		}
		return (Integer) null;
	}
	public Object GetElementAt(int row, int column)
	{
		return m_tableModel.getValueAt(row, column);
	}
	public void SetElementAt(Object aValue, int row, int column)
	{
		m_tableModel.setValueAt(aValue, row, column);
		revalidate();
		repaint();
	}
	
	public void ClearTable()
	{
		while(m_tableModel.getRowCount() > 0)
		{
			m_tableModel.removeRow(0);
		}
		revalidate();
		repaint();
	}
	
	public void SetHeaderOff()
	{
		m_table.setTableHeader(null);
		revalidate();
		repaint();
	}
	
	public void SortTable(int column, SortOrder sortOrder)
	{
		List<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
		sortKeys.add(new RowSorter.SortKey(column, sortOrder));
		m_tableSorter.setSortKeys(sortKeys);
		m_tableSorter.sort();
	}

	@Override
	public void InitializeComponents() 
	{
		Object[][] data = null;
		m_table = new JTable();
		m_tableModel = new DefaultTableModel(data, m_columnNames)
		{
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int col)
			{
				return false;
			}
		};
		ClearTable();
		m_tableSorter = new TableRowSorter<TableModel>(m_tableModel);
		m_table.setRowSorter(m_tableSorter);
		m_table = new JTable(m_tableModel);
		m_scrollPane = new JScrollPane(m_table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		m_tableTitleLabel = new JLabel(m_tableTitle);
		m_table.setEnabled(false);
	}

	@Override
	public void CreateGUI() 
	{
		setLayout(new BorderLayout());
		m_table.setFont(client.Constants.BaseFont);
		m_table.setForeground(client.Constants.blue);
		m_tableTitleLabel.setFont(client.Constants.QuestionFont);
		m_tableTitleLabel.setForeground(client.Constants.blue);
		add(m_tableTitleLabel, BorderLayout.NORTH);
		add(m_scrollPane, BorderLayout.CENTER);
	}

	@Override
	public void AddEvents() 
	{
		
	}
}
