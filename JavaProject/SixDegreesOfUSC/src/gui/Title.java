package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import client.Constants;

public class Title extends JLabel implements IGUI
{
	private static final long serialVersionUID = 1L;

	private int m_fontSize;
	private ImageIcon m_image;
	
	public Title(ImageIcon inImage, String title, int fontSize)
	{
		super(title);
		m_image = inImage;
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}
	
	@Override
	public void InitializeComponents()
	{
		setFont(Constants.TitleFont);
		setForeground(client.Constants.blue);
		setOpaque(true);
	}
	
	@Override
	public void CreateGUI()
	{
		
	}
	
	@Override
	public void AddEvents()
	{
		
	}
	
	public void setImage(ImageIcon inImage)
	{
		m_image = inImage;
		repaint();
	}
	
	public void removeImage()
	{
		m_image = null;
		repaint();
	}
	
	@Override
    protected void paintComponent(Graphics graphics) 
    {
        super.paintComponent(graphics);
        
        if (m_image != null) {
            graphics.drawImage(m_image.getImage(), 0, 0, getWidth(), getHeight(), null);
        }

    }
}
