package almonds;

public class Parse 
{
	private static String mApplicationId;
	private static String mRestAPIKey;
	
	private static final String PARSE_API_URL = "https://api.parse.com";
	private static final String PARSE_API_URL_CLASSES = "/1/classes/";
	private static final String PARSE_API_URL_USERS = "/1/users/";
	private static final String PARSE_API_URL_USERS_LOGIN = "/1/login";
	/**
	 * @param applicationId
	 * @param restAPIKey
	 */
	static public void initialize(String applicationId, String restAPIKey)
	{
		mApplicationId = applicationId;
		mRestAPIKey = restAPIKey;
	}
	
	static public String getApplicationId() {return mApplicationId;}
	static public String getRestAPIKey() {return mRestAPIKey;}
	static public String getParseAPIUrl() {return PARSE_API_URL;}
	static public String getParseAPIUrlClasses() {return getParseAPIUrl() + PARSE_API_URL_CLASSES;}
	public static String getParseAPIUrlUsers() {
		return getParseAPIUrl() + PARSE_API_URL_USERS;
	}

	public static String getParseAPIUrlUsersDelete(){
		return getParseAPIUrlUsers() + "g7y9tkhB7";
	}

	public static String getParseAPIUrlLogin(){
		return getParseAPIUrl() + PARSE_API_URL_USERS_LOGIN;
	}

	public static String getParseAPIUrlSessionTokenValidation(){
		return getParseAPIUrlUsers() + "me";
	}

}
