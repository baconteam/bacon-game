package client;

import gui.Table;

import java.awt.*;

/**
 * Created by stephenchen on 11/12/15.
 */
public class ConnectionPanel extends Table {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String mPanelTitle = "My Connections";
    private static final String [] mColumnNames = {"Student Name"};
    private static final int mTitleFontSize = Constants.Header3Size;
    private static final int mElementFontSize = Constants.Header4Size;


    public ConnectionPanel() {
        super(mPanelTitle, mColumnNames, mTitleFontSize, mElementFontSize);
        setMinimumSize(new Dimension(200,300));
        setPreferredSize(new Dimension(300,800));
        // TODO calculate and add connections between player and all users
    }


}
