package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import gui.Panel;

public class GuestMarkerPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel m_guestMarker;
	
	public GuestMarkerPanel(ImageIcon inImage) {
		super(inImage);
		m_guestMarker = new JLabel("GUEST MODE");
		m_guestMarker.setHorizontalTextPosition(JLabel.CENTER);
		m_guestMarker.setVerticalTextPosition(JLabel.CENTER);
		m_guestMarker.setFont(Constants.BaseFont.deriveFont(Font.PLAIN, 45));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(m_guestMarker);
		setMinimumSize(new Dimension(200,400));
		setPreferredSize(new Dimension(300,400));	
	}

}
