package client;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.Button;
import gui.GUILayout;
import gui.IGUI;
import gui.InputField;
import gui.Panel;
import gui.PasswordInput;

public class UserCreate extends Panel implements IGUI {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
   
    private JPanel namePanel;
    private JPanel userPanel;
    private JPanel passwordPanel;
    private JPanel confirmPanel;
    private JPanel buttonPanel; 
    
    private InputField nameField;
    private InputField userField;
    private PasswordInput passwordField;
    private PasswordInput confirmpassField;
    
    private JButton createButton;
    private JButton backButton;
    private GridBagConstraints gbc;
    
    private final int CREATE_ACTION = 0;
    private final int BACK_ACTION = 1;
    
    public UserCreate(ActionListener[] actionListeners, ImageIcon inImage) {
    	super(actionListeners, inImage);
    }

    public String GetName()
    {
    	return nameField.GetInput();
    }
    public String GetUser()
    {
    	return userField.GetInput();
    }
    public String GetPassword()
    {
    	return passwordField.GetPassword();
    }
    public String GetPasswordConfirm()
    {
    	return confirmpassField.GetPassword();
    }
    public void ClearField()
    {
    	nameField.RemoveText();
    	userField.RemoveText();
    	passwordField.RemoveText();
    	confirmpassField.RemoveText();
    }
	@Override
	public void InitializeComponents() {
		// TODO Auto-generated method stub
        nameField = new InputField("Enter Name:", GUILayout.Vertical, 25, 30);
        userField = new InputField("Enter Username:", GUILayout.Vertical, 25, 30);
        passwordField = new PasswordInput("Enter Password:", GUILayout.Vertical, 25, 30);
        confirmpassField = new PasswordInput("Confirm Password:", GUILayout.Vertical, 25, 30);
        
        createButton = new Button("Create", Constants.Header2Size);
        createButton.setSize(100, 30);
        backButton = new Button("Back", Constants.Header2Size);
        backButton.setSize(100,30);
        
        namePanel = new JPanel();
        namePanel.setLayout(new BoxLayout(namePanel, BoxLayout.X_AXIS));
        namePanel.add(nameField);
        namePanel.setBorder(new EmptyBorder(30, 30, 30, 30));
        
        userPanel = new JPanel();
        userPanel.setLayout(new BoxLayout(userPanel, BoxLayout.X_AXIS));
        userPanel.add(userField);
        userPanel.setBorder(new EmptyBorder(30, 30, 30, 30));
        
        passwordPanel = new JPanel();
        passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.X_AXIS));
        passwordPanel.add(passwordField);
        passwordPanel.setBorder(new EmptyBorder(30, 30, 30, 30));
        
        confirmPanel = new JPanel();
        confirmPanel.setLayout(new BoxLayout(confirmPanel, BoxLayout.X_AXIS));
        confirmPanel.add(confirmpassField);
        confirmPanel.setBorder(new EmptyBorder(30, 30, 30, 30));
        
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
	}

	@Override
	public void CreateGUI() {
		// TODO Auto-generated method stub
		gbc.gridwidth = 1;
        gbc.ipadx = 100;
        gbc.gridx = 1;
        gbc.ipady = 30;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridy = 2;
        gbc.insets = new Insets(0,80,40,40);
        buttonPanel.add(createButton,gbc);
        gbc.gridx = 2;
        gbc.insets = new Insets(0,40,40,40);
        buttonPanel.add(backButton,gbc);
        
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setSize(250,400);
        add(namePanel);
        add(userPanel);
        add(passwordPanel);
        add(confirmPanel);
        add(buttonPanel);
        setBorder(new EmptyBorder(70, 70, 70, 70));
	}

	@Override
	public void AddEvents() {
		// TODO Auto-generated method stub
		createButton.addActionListener(GetActionListener(CREATE_ACTION));
		backButton.addActionListener(GetActionListener(BACK_ACTION));
	}
}
