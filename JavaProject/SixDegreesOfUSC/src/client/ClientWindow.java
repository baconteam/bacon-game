package client;

import javax.swing.*;

/**
 * Created by stephenchen on 11/5/15.
 */
public class ClientWindow extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClientWindow() {
        super("Six Degrees of USC");
        initializeComponents();
        createGUI();
        addEvents();
    }

    public void initializeComponents() {

    }

    public void createGUI() {
        setSize(500, 600);

    }

    public void addEvents() {

    }

    public static void main(String [] args) {
        ClientWindow cw = new ClientWindow();
        cw.setVisible(true);
    }
}
