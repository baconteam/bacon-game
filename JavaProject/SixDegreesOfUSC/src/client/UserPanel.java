package client;

import gui.Button;
import gui.IGUI;
import gui.Panel;
import gui.Title;
import messages.LoginUserMessage;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import database.ParseDatabase;

/**
 * Created by stephenchen on 11/14/15.
 */
public class UserPanel extends Panel implements IGUI {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private gui.Button m_logoutButton;
    private Title m_usernameLabel;
    private String username;
    private JPanel buttonPanel;
    private GridBagConstraints gbc;
    protected Button m_addClassButton;
    
    public UserPanel(ImageIcon inImage, String username) {
        super(inImage, true);
        this.username = username;

    }

    @Override
    public void InitializeComponents() {
        m_logoutButton = new gui.Button("Logout", Constants.Header2Size);
        m_usernameLabel = new Title(null, username, Constants.Header2Size);
        m_addClassButton = new Button("Add Class", Constants.Header2Size);
        buttonPanel = new JPanel(new GridBagLayout());
        gbc = new GridBagConstraints();
    }

    @Override
    public void CreateGUI() {
        add(m_usernameLabel);
        gbc.gridwidth = 1;
        gbc.ipadx = 60;
        gbc.ipady = 30;
        buttonPanel.add(m_logoutButton, gbc);
        gbc.insets = new Insets(20, 20, 20, 20);
        gbc.gridy = 1;
        buttonPanel.add(m_addClassButton, gbc);
        add(buttonPanel);
        setMinimumSize(new Dimension(200,200));
        setPreferredSize(new Dimension(300,200));
    }

    @Override
    public void AddEvents() {
    	m_logoutButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(ParseDatabase.GetUsername() != null)
					ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new LoginUserMessage(ParseDatabase.GetUsername(),"", false));
				ParseDatabase.Logout();
				
				ClientPanel.GetClientPanel().GetRegUserGamePanel().GetChatPanel().ClearField();
				ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().ClearTable();
				ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().ClearTable();

				ClientPanel.GetClientPanel().ClearAll();
				
				ClientPanel.GetClientPanel().removeAll();
				ClientPanel.GetClientPanel().add(ClientPanel.GetClientPanel().GetMainMenuPanel());
				ClientPanel.GetClientPanel().revalidate();
				ClientPanel.GetClientPanel().repaint();
			}
    	});
    	
    	m_addClassButton.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			ClientPanel.GetClientPanel().removeAll();
    			ClientPanel.GetClientPanel().add(ClientPanel.GetClientPanel().GetAddClassPanel());
    			ClientPanel.GetClientPanel().revalidate();
    			ClientPanel.GetClientPanel().repaint();
    		}
    	});
    }
}
