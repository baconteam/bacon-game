package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import database.ParseDatabase;
import messages.ConnectionMessage;
import messages.LoginUserMessage;
import messages.ScoreMessage;
import util.Debug;

public class ClientPanel extends JPanel {

	private ClientConnectPanel clientConnectPanel;
	private MainMenu mainMenuPanel;
	private LoginPanel loginPanel;
	private AddClassPanel addClassPanel;
	private UserCreate userCreatePanel;
	private RegUserGamePanel regGamePanel;
	private AnonUserGamePanel anonGamePanel;
	private ImageIcon backgroundImage;
    private static final long serialVersionUID = -8108212759011221387L;
    private SixDegreesClient m_client;

    private static ClientPanel s_clientPanel = null;
    public static ClientPanel GetClientPanel() { return s_clientPanel; }
    
    public MainMenu GetMainMenuPanel() { return mainMenuPanel; }
    public SixDegreesClient GetClient() { return m_client; }
    public RegUserGamePanel GetRegUserGamePanel() { return regGamePanel; }
    public LoginPanel GetLoginPanel() { return loginPanel; }
    public AnonUserGamePanel GetAnonUserGamePanel() { return anonGamePanel; }
    public AddClassPanel GetAddClassPanel() { return addClassPanel;}
    {
    	if(s_clientPanel == null)
    	{
    		s_clientPanel = this;
    	}
    	
    	try {
			backgroundImage = new ImageIcon(ImageIO.read(new File("images/border.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	clientConnectPanel = new ClientConnectPanel(new ActionListener[] {new ActionListener(){
    		//connect to port
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(!clientConnectPanel.GetHostname().isEmpty()) {
					try {
						m_client = new SixDegreesClient(clientConnectPanel.GetHostname(), clientConnectPanel.GetPort());
						m_client.start();
						while(!m_client.GetConnected() && !m_client.DidConnectionFail())
						{
							Thread.sleep(1);
						}
						if(m_client.GetConnected()){
							ClientPanel.this.removeAll();
							ClientPanel.this.add(mainMenuPanel);
							ClientPanel.this.revalidate();
							ClientPanel.this.repaint();
						} else {
							JOptionPane.showMessageDialog(ClientPanel.this, "Not connected to server yet",
									"Please try again", JOptionPane.ERROR_MESSAGE);
						}
					} catch(NumberFormatException nfe) {
						System.out.println("nfe is " + nfe.getMessage());
						JOptionPane.showMessageDialog(ClientPanel.this, "Incorrect port",
								"Please try again", JOptionPane.ERROR_MESSAGE);
					} catch(InterruptedException ie)
					{
						Debug.DebugLog(ie.getMessage());
					}
				} else {
					JOptionPane.showMessageDialog(ClientPanel.this, "Empty field",
							"Please try again", JOptionPane.ERROR_MESSAGE);
				}
			}
    		
    	}}, backgroundImage);
    	ParseDatabase.Init();
		refreshComponents();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(clientConnectPanel);
    }
    public void ClearAll()
    {
    	addClassPanel.ClearField();
    	loginPanel.ClearField();
    	userCreatePanel.ClearField();
    }
    //creating all panels within the client
    private void refreshComponents(){
    	mainMenuPanel = new MainMenu(new ActionListener[] {new ActionListener(){
    		//login action
			@Override
			public void actionPerformed(ActionEvent e) {
				ClientPanel.this.removeAll();
				ClientPanel.this.add(loginPanel);
				ClientPanel.this.revalidate();
				ClientPanel.this.repaint();
			}
    		
    	}, new ActionListener(){
    		//guest action
			@Override
			public void actionPerformed(ActionEvent e) {
				ClientPanel.this.removeAll();
				ClientPanel.this.add(anonGamePanel);
				ClientPanel.this.revalidate();
				ClientPanel.this.repaint();
			}
    		//images
    	}}, backgroundImage);
    	
    	loginPanel = new LoginPanel(new ActionListener[] {new ActionListener(){
    		//login action
			@Override
			public void actionPerformed(ActionEvent e) {
				String username = loginPanel.GetUsername();
				String password = loginPanel.GetPassword();
				if(ParseDatabase.Login(username, password) && !username.isEmpty() && !password.isEmpty())
				{
		    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new LoginUserMessage(ParseDatabase.GetUsername(),"", true));
		    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetChatPanel().AddUserToUsernamePanel(ParseDatabase.GetUsername());
		    		Vector<String> connections = new Vector<String>();
		    		connections = ParseDatabase.GetAllConnections();
		    		
		    		for(String c : connections)
		    		{
		    			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().AddElement(new Object[] {c});
		    		}
//		    		//Vector<Vector<String> > GetUsersSortedByScore();
//		    		Vector<Vector<String> > userAndScores = ParseDatabase.GetUsersSortedByScore();
//		    		
//		    		for(int i=0; i<userAndScores.size(); ++i)
//		    		{
//		    			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().
//		    			AddElement(new Object[] {userAndScores.elementAt(i).elementAt(0),
//		    									userAndScores.elementAt(i).elementAt(2)});
//		    		}
		    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().AddScoreToUser(ParseDatabase.GetUsername());
		    		System.out.println("Login Successful!");
					ClientPanel.this.removeAll();
					// ClientPanel.this.add(addClassPanel);
					ClientPanel.this.add(regGamePanel);
					ClientPanel.this.revalidate();
					ClientPanel.this.repaint();
				}
				else
				{
					System.out.println("Login Failed!");
					JOptionPane.showMessageDialog(ClientPanel.this, "Incorrect Password or Username",
							"Please try again", JOptionPane.ERROR_MESSAGE);
					loginPanel.ClearField();
				}
			}
    		
    	}, new ActionListener(){
    		//register action
			@Override
			public void actionPerformed(ActionEvent e) {
				ClientPanel.this.removeAll();
				ClientPanel.this.add(userCreatePanel);
				ClientPanel.this.revalidate();
				ClientPanel.this.repaint();
			}
    		
    	}, new ActionListener() {
    		@Override 
    		public void actionPerformed(ActionEvent e) {
    			ClearAll();
    			ClientPanel.this.removeAll();
    			ClientPanel.this.add(mainMenuPanel);
    			ClientPanel.this.revalidate();
    			ClientPanel.this.repaint();
    		}
    	}}, backgroundImage);
    	
    	addClassPanel = new AddClassPanel(new ActionListener[] {new ActionListener(){
    		//create action
			@Override
			public void actionPerformed(ActionEvent e) {
				try
				{
					int semester = Integer.parseInt(addClassPanel.GetSemester());
					String classid = addClassPanel.GetClassID();
					
					if(ParseDatabase.AddClass(classid, semester))
					{
						ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().ClearTable();
			    		Vector<String> connections = new Vector<String>();
			    		connections = ParseDatabase.GetAllConnections();
			    		
			    		for(String c : connections)
			    		{
			    			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().AddElement(new Object[] {c});
			    		}
			    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ConnectionMessage(ParseDatabase.GetUsername(), ""));

			    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().ClearTable();
			    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().AddScoreToUser(ParseDatabase.GetUsername());
			    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ScoreMessage(ParseDatabase.GetUsername(),""));
						ClientPanel.this.removeAll();
						ClientPanel.this.add(regGamePanel);
						ClientPanel.this.revalidate();
						ClientPanel.this.repaint();
					}
					else
					{
						System.out.println("Failed to add class!");
						JOptionPane.showMessageDialog(ClientPanel.this, "Empty field or incorrect class",
								"Please try again", JOptionPane.ERROR_MESSAGE);
					}
				}
				catch(NumberFormatException e1)
				{
					System.out.println("Please enter only integers in the semester field.");
				}
			}
    		
    	}, new ActionListener(){
    		//continue action
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().ClearTable();
	    		Vector<String> connections = new Vector<String>();
	    		connections = ParseDatabase.GetAllConnections();
	    		
	    		for(String c : connections)
	    		{
	    			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().AddElement(new Object[] {c});
	    		}
	    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ConnectionMessage(ParseDatabase.GetUsername(), ""));

	    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().ClearTable();
	    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().AddScoreToUser(ParseDatabase.GetUsername());
	    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ScoreMessage(ParseDatabase.GetUsername(),""));
				ClientPanel.this.removeAll();
				ClientPanel.this.add(regGamePanel);
				ClientPanel.this.revalidate();
				ClientPanel.this.repaint();
			}
    		//images
    	}, new ActionListener() {
    		//back action
    		@Override
    		public void actionPerformed(ActionEvent e) {
				ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().ClearTable();
	    		Vector<String> connections = new Vector<String>();
	    		connections = ParseDatabase.GetAllConnections();
	    		
	    		for(String c : connections)
	    		{
	    			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().AddElement(new Object[] {c});
	    		}
	    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ConnectionMessage(ParseDatabase.GetUsername(), ""));

	    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().ClearTable();
	    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().AddScoreToUser(ParseDatabase.GetUsername());
	    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ScoreMessage(ParseDatabase.GetUsername(),""));
    			ClearAll();
    			ClientPanel.this.removeAll();
    			ClientPanel.this.add(loginPanel);
    			ClientPanel.this.revalidate();
    			ClientPanel.this.repaint();
    		}
    	}}, backgroundImage);
    	
    	//action for user panel
    	userCreatePanel = new UserCreate(new ActionListener[] {new ActionListener(){
    		//create action
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = userCreatePanel.GetName();
				String username = userCreatePanel.GetUser();
				String password = userCreatePanel.GetPassword();
				String passwordConfirm = userCreatePanel.GetPasswordConfirm();
				
				if(password.equals(passwordConfirm) && !name.isEmpty() && !username.isEmpty() && !password.isEmpty() && !passwordConfirm.isEmpty()) {
					if(ParseDatabase.AddUser(username, password, name))
					{
			    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new LoginUserMessage(ParseDatabase.GetUsername(),"", true));
			    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetChatPanel().AddUserToUsernamePanel(ParseDatabase.GetUsername());
//			    		Vector<String> connections = new Vector<String>();
//			    		connections = ParseDatabase.GetAllConnections();
//			    		
//			    		for(String c : connections)
//			    		{
//			    			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().AddElement(new Object[] {c});
//			    		}
//			    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ConnectionMessage(ParseDatabase.GetUsername(), ""));
//
//			    		//Vector<Vector<String> > GetUsersSortedByScore();
//			    		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().AddScoreToUser(ParseDatabase.GetUsername());
//			    		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ScoreMessage(ParseDatabase.GetUsername(),""));
						System.out.println("User Creation Successful");
						ClientPanel.this.removeAll();
						ClientPanel.this.add(addClassPanel);
						ClientPanel.this.revalidate();
						ClientPanel.this.repaint();
					}
					else
					{
						System.out.println("User Creation Unsuccessful!");
						JOptionPane.showMessageDialog(ClientPanel.this, "Incorrect/Empty Password or Username",
								"Please try again", JOptionPane.ERROR_MESSAGE);
					}
				}
				else
				{
					System.out.println("Could not confirm password!");
				}
			}
    		
    	}, new ActionListener(){
    		//back action
			@Override
			public void actionPerformed(ActionEvent e) {
				ClientPanel.this.removeAll();
				ClientPanel.this.add(loginPanel);
				ClientPanel.this.revalidate();
				ClientPanel.this.repaint();
			}
    		//images
    	}}, backgroundImage);
    	regGamePanel = new RegUserGamePanel(null);
    	anonGamePanel = new AnonUserGamePanel(null);
    }
}
