package client;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.Button;
import gui.GUILayout;
import gui.IGUI;
import gui.InputField;
import gui.Panel;

public class AddClassPanel extends Panel implements IGUI{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Button addButton, continueButton, backButton;
	private InputField classIDField, semesterField;
	private JPanel semesterPanel, classPanel, buttonPanel;
	private JLabel classTipLabel,toolTipLabel;
	private GridBagConstraints gbc;
	
	private final int ADD_ACTION = 0;
	private final int BACK_ACTION = 2;
	private final int CONTINUE_ACTION = 1;
	
	public AddClassPanel(ActionListener[] actionListeners, ImageIcon inImage) {
		super(actionListeners, inImage);
	}

	@Override
	public void InitializeComponents() {
		// TODO Auto-generated method stub
		addButton = new Button("Add Class", Constants.Header2Size);
		continueButton = new Button("Continue", Constants.Header2Size);
		backButton = new Button("Back", Constants.Header2Size);
		
		classIDField = new InputField("Class ID: ", GUILayout.Vertical, 25, 30);
		classTipLabel = new JLabel("Class ID with no spaces Ex: CSCI201");
		classTipLabel.setFont(Constants.BaseFont);
		classTipLabel.setForeground(Constants.blue);
		semesterField = new InputField("Semester: ", GUILayout.Vertical, 25, 30);
		toolTipLabel = new JLabel("1 = Spring, 2 = Fall, 3 = Summer. Year + Semester. Ex: 20151 = Spring 2015");
		toolTipLabel.setFont(Constants.BaseFont);
		toolTipLabel.setForeground(Constants.blue);
		
		classPanel = new JPanel(new BorderLayout());
		semesterPanel = new JPanel(new BorderLayout());
		buttonPanel = new JPanel(new GridBagLayout());
		gbc = new GridBagConstraints();
	}

	@Override
	public void CreateGUI() {
		// TODO Auto-generated method stub
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		classPanel.add(classIDField);
		classPanel.add(classTipLabel, BorderLayout.SOUTH);
		classPanel.setBorder(new EmptyBorder(50, 50, 50, 50));
		
		semesterPanel.add(semesterField);
		semesterPanel.add(toolTipLabel, BorderLayout.SOUTH);
		semesterPanel.setBorder(new EmptyBorder(50, 50, 50, 50));
		
		gbc.gridwidth = 1;
        gbc.ipadx = 100;
        gbc.gridx = 1;
        gbc.ipady = 30;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridy = 2;
        gbc.insets = new Insets(0,40,40,40);
        buttonPanel.add(addButton,gbc);
        gbc.gridx = 2;
        gbc.insets = new Insets(0,40,40,40);
        buttonPanel.add(continueButton,gbc);
        gbc.gridx = 3;
        buttonPanel.add(backButton,  gbc);
        
        add(classPanel);
        add(semesterPanel);
        add(buttonPanel);
        setBorder(new EmptyBorder(80, 80, 80, 80));
	}

	@Override
	public void AddEvents() {
		// TODO Auto-generated method stub
		addButton.addActionListener(GetActionListener(ADD_ACTION));
		continueButton.addActionListener(GetActionListener(CONTINUE_ACTION));
		backButton.addActionListener(GetActionListener(BACK_ACTION));
	}

	public String GetSemester() {
		return semesterField.GetInput();
	}
	
	public String GetClassID() {
		return classIDField.GetInput();
	}
	public void ClearField(){
		semesterField.RemoveText();
		classIDField.RemoveText();
	}
}
