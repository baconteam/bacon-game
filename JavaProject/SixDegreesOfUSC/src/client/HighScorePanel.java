package client;

import gui.Table;

import java.awt.*;
import java.util.Vector;

import javax.swing.SortOrder;

import database.ParseDatabase;

/**
 * Created by stephenchen on 11/13/15.
 */
public class HighScorePanel extends Table {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String mPanelTitle = "High Scores";
    private static final String [] mColumnNames = {"Name", "Score"};
    private static final int mTitleFontSize = Constants.Header3Size;
    private static final int mElementFontSize = Constants.Header4Size;


    public HighScorePanel() {
        super(mPanelTitle, mColumnNames, mTitleFontSize, mElementFontSize);
        setMinimumSize(new Dimension(200,300));
        setPreferredSize(new Dimension(300, 800));
        // TODO add All High Scores and users
    }
    public void AddScoreToUser(String username)//, int scores)
    {
    	this.ClearTable();
    	Vector<Vector<String> > userAndScores = ParseDatabase.GetUsersSortedByScore();
    	
		for(int i=0; i<userAndScores.size(); ++i)
		{
			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().
			AddElement(new Object[] {userAndScores.elementAt(i).elementAt(0),
									userAndScores.elementAt(i).elementAt(2)});
		}
    	//Vector<Vector<String> > GetUsersSortedByScore()
//    	if(this.ElementExist(username, 0))
//    	{
//        	int row = this.FindElementRow(username, 0);
//        	Object currentScore = this.GetElementAt(row, 1);
//        	int totalScore = (int) currentScore + scores;
//        	this.SetElementAt(totalScore, row, 1);
//    	}
//    	else
//    	{
//    		this.AddElement(new Object[] { username, scores});
//    	}
//    	this.SortTable(1, SortOrder.DESCENDING);
    	
    }
}
