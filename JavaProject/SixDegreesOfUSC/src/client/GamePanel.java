package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Random;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.ParseDatabase;
import gui.IGUI;
import gui.Panel;
import gui.Question;
import gui.Title;
import messages.LoginUserMessage;
import messages.ScoreMessage;
import util.Debug;

/**
 * Created by stephenchen on 11/14/15.
 */
public class GamePanel extends Panel implements IGUI {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Title m_gameLabel;
	private Question m_question;
	private JPanel questionPanel, titlePanel;
	
	private ActionListener m_submitQuestionAction;
	private ActionListener m_requestNewQuestionAction;
	
	
	private final int SUBMIT_ACTION = 0;
	private final int NEW_QUESTION_ACTION = 1;
	private Random m_random;
	
    public GamePanel(ActionListener[] actionListeners, ImageIcon inImage) 
    {
        super(actionListeners, inImage);
        
        m_submitQuestionAction = GetActionListener(SUBMIT_ACTION);
        m_requestNewQuestionAction = GetActionListener(NEW_QUESTION_ACTION);
        m_random = new Random();
        
        PostInitializeComponents();
        PostCreateGUI();
        PostAddEvents();
    }
    
    public void SubmitAnswer()
    {
    	Debug.DebugLog("Submit question action");
		if(m_question.IsCorrectAnswerSelected())
		{
			JOptionPane.showMessageDialog(GamePanel.this, "The correct answer was selected!", "Success!", JOptionPane.PLAIN_MESSAGE);
			ParseDatabase.incrementScore();
			//ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ScoreMessage(ParseDatabase.GetUsername(), "", 1));
		}
		else
		{
			String answer = m_question.GetCorrectAnswer();
			JOptionPane.showMessageDialog(GamePanel.this, "Nope. It was " + answer, "#Bye", JOptionPane.ERROR_MESSAGE);
			ParseDatabase.decrementScore();
			//ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ScoreMessage(ParseDatabase.GetUsername(), "", -1));
		}
		
		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().AddScoreToUser(ParseDatabase.GetUsername());
		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ScoreMessage(ParseDatabase.GetUsername(),""));
		RequestNewQuestion();

    }
    
    public void RequestNewQuestion()
    {
    	QuestionSetUp();
    }
    
    private void PostInitializeComponents()
    {
    	m_gameLabel = new Title(null, "game panel", Constants.Header3Size);
    }
    
    private void PostCreateGUI()
    {
    	setLayout(new BorderLayout());
    	JPanel titlePanel = new JPanel();
    	titlePanel.add(m_gameLabel);
    	titlePanel.setBorder(new EmptyBorder(0, 50, 0, 50));
        add(titlePanel, BorderLayout.NORTH);
        setBackground(Color.DARK_GRAY);
        setMinimumSize(new Dimension(300,600));
        setPreferredSize(new Dimension(300,600));
        QuestionSetUp();
    }
    
    private void PostAddEvents()
    {
    	
    }
    
    private void Revalidate()
    {
    	questionPanel.revalidate();
    	questionPanel.repaint();
    }
    
    private void QuestionSetUp() 
    {
    	if(m_question != null)
    	{
        	questionPanel.remove(m_question);
        	m_question = null;
    	}
    	
    	String luckyContestant = "";
    	HashMap<String, Vector<String>> allUsers = ParseDatabase.GetAllUserClasses();
    	while(luckyContestant.equals(""))
    	{
    		int numberOfUsersToPickFrom = allUsers.keySet().size();
    		Object[] users = allUsers.keySet().toArray();
    		String user = (String)users[m_random.nextInt(numberOfUsersToPickFrom)];
    		if(user != ParseDatabase.GetUsername() && !allUsers.get(user).isEmpty())
    		{
    			luckyContestant = user;
    			break;
    		}
    	}
    	String myQuestion = "";
    	Vector<String> classesForLuckyContestant = allUsers.get(luckyContestant);
    	for(int i = 0; i < classesForLuckyContestant.size(); ++i)
    	{
    		myQuestion += System.lineSeparator() + (i + 1) + classesForLuckyContestant.get(i);
    	}
    	int numberOfOtherStudents = 3;
    	Vector<String> otherStudents = new Vector<String>();
    	for(int i = 0; i < numberOfOtherStudents; ++i)
    	{
    		String otherUser = "";
    		while(otherUser.equals(""))
    		{
    			int numberOfUsersToPickFrom = allUsers.keySet().size();
        		Object[] users = allUsers.keySet().toArray();
        		String user = (String)users[m_random.nextInt(numberOfUsersToPickFrom)];
	    		if(!user.equals(ParseDatabase.GetUsername()) && !user.equals(luckyContestant) && !otherStudents.contains(user))
	    		{
	    			otherUser = user;
	    			break;
	    		}
    		}
    		otherStudents.add(otherUser);
    	}
    	
    	Debug.DebugLog(luckyContestant);
    	
    	questionPanel = new JPanel(new GridBagLayout());
    	GridBagConstraints gbc = new GridBagConstraints();
    	m_question = new Question(classesForLuckyContestant, otherStudents, luckyContestant, new ActionListener[] { m_requestNewQuestionAction, m_submitQuestionAction });
    	gbc.fill = GridBagConstraints.BOTH;
    	gbc.weightx = 1.0;
    	gbc.weighty = 1.0;
    	questionPanel.add(m_question, gbc);
    	add(questionPanel,BorderLayout.CENTER);
    	Revalidate();
    }
}
