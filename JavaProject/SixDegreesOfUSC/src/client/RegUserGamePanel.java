package client;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import gui.IGUI;
import gui.Panel;

/**
 * Created by stephenchen on 11/14/15.
 */
public class RegUserGamePanel extends Panel implements IGUI{
    /**
	 * 
	 */
	protected static final long serialVersionUID = 1L;
	protected UserPanel m_userPanel;
    protected HighScorePanel m_highscorePanel;
    protected ConnectionPanel m_connectionPanel;
    protected GamePanel m_gamePanel;
    protected ChatPanel m_chatPanel;
    protected Panel m_topPanel;
    protected Panel m_topMidRightPanel;
    protected Panel m_topRightPanel;
    protected Panel m_bottomPanel;
    
    public RegUserGamePanel(ImageIcon inImage) {
        super(inImage);
    }

    public ChatPanel GetChatPanel() { return m_chatPanel; }
    public HighScorePanel GetHighScorePanel() { return m_highscorePanel;}
    public ConnectionPanel GetConnectionPanel() {return m_connectionPanel;}
    public GamePanel GetGamePanel() { return m_gamePanel; }

    @Override
    public void InitializeComponents() {
    	m_topPanel = new Panel(null);
    	m_topMidRightPanel = new Panel(null);
    	m_topRightPanel = new Panel(null);
    	m_bottomPanel = new Panel(null);
        m_userPanel = new UserPanel(null, "Stephen");
        m_highscorePanel = new HighScorePanel();
        m_connectionPanel = new ConnectionPanel();
        m_gamePanel = new GamePanel(new ActionListener[] {
        		new ActionListener()
        		{
        			@Override
        			public void actionPerformed(ActionEvent actionEvent)
        			{
        				m_gamePanel.SubmitAnswer();
        			}
        		},
        		
        		new ActionListener()
        		{
        			@Override
        			public void actionPerformed(ActionEvent actionEvent)
        			{
        				m_gamePanel.RequestNewQuestion();
        			}
        		}
    		}, null);
        m_chatPanel = new ChatPanel();
    }

    @Override
    public void CreateGUI() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        m_topMidRightPanel.setLayout(new BoxLayout(m_topMidRightPanel, BoxLayout.Y_AXIS));
        m_topMidRightPanel.add(m_highscorePanel);
        m_topMidRightPanel.add(m_connectionPanel);
      	
        m_topRightPanel.setLayout(new BoxLayout(m_topRightPanel, BoxLayout.X_AXIS));
        m_topRightPanel.add(m_topMidRightPanel);
        m_topRightPanel.add(m_userPanel);
        m_topRightPanel.setMinimumSize(new Dimension(150,500));
      	m_topRightPanel.setPreferredSize(new Dimension(150,500));
      	
        m_topPanel.setLayout(new BoxLayout(m_topPanel, BoxLayout.X_AXIS));
        m_topPanel.add(m_gamePanel);
        m_topPanel.add(m_topRightPanel);
        m_topPanel.setMinimumSize(new Dimension(300,500));
      	m_topPanel.setPreferredSize(new Dimension(400,500));
        
        add(m_topPanel);
        add(m_chatPanel);
    }

    @Override
    public void AddEvents() {
    }
}
