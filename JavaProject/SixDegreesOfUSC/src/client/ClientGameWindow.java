package client;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import database.ParseDatabase;
import gui.IGUI;
import messages.LoginUserMessage;

/**
 * Created by stephenchen on 11/5/15.
 */
public class ClientGameWindow extends JFrame implements IGUI{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ClientPanel clientPanel;
    public ClientGameWindow() {
        super("Six Degrees of USC");
        InitializeComponents();
        CreateGUI();
        AddEvents();
    }

    @Override
    public void InitializeComponents() {
        clientPanel = new ClientPanel();
    }

    @Override
    public void CreateGUI() {
        setSize(new Dimension(1000, 750));
        setMinimumSize(new Dimension(1000,750));
//        setMaximumSize(new Dimension(1000,600));
        add(clientPanel);
    }

    @Override
    public void AddEvents() {
    	setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    	
    	addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent windowEvent)
			{
				if(ParseDatabase.GetUsername() != null)
					ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new LoginUserMessage(ParseDatabase.GetUsername(),"", false));
				if(ClientPanel.GetClientPanel().GetClient() != null)
					ClientPanel.GetClientPanel().GetClient().Disconnect();
			}
		});
    }

    public static void main(String [] args) {
        ClientGameWindow cw = new ClientGameWindow();
        cw.setVisible(true);
    }
}
