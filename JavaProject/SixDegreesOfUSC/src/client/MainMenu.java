package client;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.Button;
import gui.IGUI;
import gui.Panel;

public class MainMenu extends Panel implements IGUI {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Button login, guest;
    private Panel titlePanel;
    private ImageIcon titleImage;
    private GridBagConstraints gbc;
    
    private final int LOGIN_ACTION = 0;
    private final int GUEST_ACTION = 1;
    
    public MainMenu(ActionListener[] actionListeners, ImageIcon inImage) {
    	super(actionListeners, inImage);      
    }
    
    @Override
    public void InitializeComponents() {
    	login = new Button("Login", Constants.Header2Size);        
        guest = new Button("Guest Login", Constants.Header2Size);
        titleImage = null;
		try {
			titleImage = new ImageIcon(ImageIO.read(new File("images/logo2.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
        titlePanel = new Panel(titleImage);
        gbc = new GridBagConstraints();
    }
    
    @Override 
    public void CreateGUI() {
    	setLayout(new GridBagLayout());
        gbc.ipadx = titleImage.getImage().getWidth(null);
        gbc.ipady = titleImage.getImage().getHeight(null);
        gbc.insets = new Insets(40,40,40,40);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridy = 1;
        gbc.gridwidth = 3;
        add(titlePanel,gbc);
        gbc.gridwidth = 1;
        gbc.ipadx = 100;
        gbc.gridx = 1;
        gbc.ipady = 30;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridy = 2;
        gbc.insets = new Insets(40,80,40,40);
        add(login,gbc);
        gbc.gridx = 2;
        gbc.insets = new Insets(40,40,40,40);
        add(guest,gbc);
        setSize(600,500);
        setBorder(new EmptyBorder(80, 80, 80, 80));
    }
    
    @Override
    public void AddEvents() {
    	login.addActionListener(GetActionListener(LOGIN_ACTION));
    	guest.addActionListener(GetActionListener(GUEST_ACTION));
    }
}
