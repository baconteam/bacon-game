package client;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.Button;
import gui.GUILayout;
import gui.IGUI;
import gui.InputField;
import gui.Panel;

public class ClientConnectPanel extends Panel implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private InputField m_portField;
	private InputField m_hostField;
	private Button m_connectButton;
	private JPanel m_buttonPanel;
	
	private final int CONNECT = 0;
	
	public ClientConnectPanel(ActionListener[] actionListeners, ImageIcon inImage) 
	{
		super(actionListeners, inImage);
	}
	
	public String GetHostname()
	{
		return m_hostField.GetInput();
	}
	
	public int GetPort()
	{
		return Integer.parseInt(m_portField.GetInput());
	}
	
	@Override
	public void InitializeComponents()
	{
		m_portField = new InputField("Port:", GUILayout.Vertical, Constants.Header2Size, Constants.Header2Size);
		m_hostField = new InputField("Hostname:", GUILayout.Vertical, Constants.Header2Size, Constants.Header2Size);
		m_connectButton = new Button("Connect", Constants.Header2Size);
		m_buttonPanel = new JPanel(new GridBagLayout());
	}
	
	@Override
	public void CreateGUI()
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		m_portField.setBorder(new EmptyBorder(30, 30, 30, 30));
		m_hostField.setBorder(new EmptyBorder(30, 30, 30, 30));
		
		m_connectButton.setSize(100, 30);
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.ipadx = 100;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.ipady = 30;
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.insets = new Insets(40,40,40,40);
		m_buttonPanel.add(m_connectButton, gridBagConstraints);
		
		add(m_hostField);
		add(m_portField);
		add(m_buttonPanel);
		add(Box.createVerticalGlue());

		setSize(600, 400);
		setBorder(new EmptyBorder(60, 60, 60, 60));
		setLocation(0, 0);
		setVisible(true);
	}
	
	@Override
	public void AddEvents()
	{
		m_connectButton.addActionListener(GetActionListener(CONNECT));
	}
}
