package client;

import java.awt.Color;
import java.awt.Font;

import util.FontLibrary;

public class Constants 
{
	public static Font BaseFont = FontLibrary.getFont("fonts/ASENINE_.ttf", Font.PLAIN, 20);
	public static Font QuestionFont = FontLibrary.getFont("fonts/ASENINE_.ttf", Font.PLAIN, 25);
	public static Font TitleFont = FontLibrary.getFont("fonts/ASENINE_.ttf", Font.PLAIN, 35);
	
	public static int Header1Size = 36;
	public static int Header2Size = 24;
	public static int Header3Size = 16;
	public static int Header4Size = 12;
	
	public static Color orange = new Color(255,189,73);
	public static Color blue = new Color(3, 63, 204);
}
