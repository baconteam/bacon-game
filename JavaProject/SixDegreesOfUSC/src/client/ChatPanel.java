package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import database.ParseDatabase;
import gui.Button;
import gui.Table;
import messages.ChatMessage;

public class ChatPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 836487230672421423L;
	private JTextArea textBox;
	private JScrollPane scroll;
	private JTextField chatField;
	private JButton enterChatButton;
	private JPanel buttonPanel;
	private Table usernamePanel;
	
	public ChatPanel()
	{		
		textBox = new JTextArea(5,20);
	    textBox.setLineWrap(true);
	    textBox.setWrapStyleWord(true);
	    textBox.setEditable(false);
	    textBox.setFont(Constants.BaseFont);
	    textBox.setBackground(Constants.blue);
	    textBox.setBorder(new LineBorder(Constants.orange, 2));
	    textBox.setForeground(Color.WHITE);

		scroll = new JScrollPane(textBox);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.X_AXIS));
		chatField = new JTextField();

		buttonPanel = new JPanel(new GridBagLayout());
	    GridBagConstraints gbc = new GridBagConstraints();
	    gbc.ipadx = 900;
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.anchor = GridBagConstraints.WEST;
	    gbc.gridx = 0;
	    gbc.weightx = 1;
	    gbc.weighty = 1;
		chatField.setFont(Constants.BaseFont);
		chatField.setBackground(Constants.blue);
	    chatField.setBorder(new LineBorder(Constants.orange, 2));
	    chatField.setForeground(Color.WHITE);
	  
	    buttonPanel.add(chatField, gbc);
	    gbc.gridx = 1;
	    gbc.weightx = 0;
	    gbc.ipadx = 50;
		enterChatButton = new Button("Enter", Constants.Header2Size);		
		buttonPanel.add(enterChatButton, gbc);
		southPanel.add(buttonPanel);
		
		String[] columnName = {"Username"};
		usernamePanel = new Table("Users", columnName, Constants.Header3Size,Constants.Header4Size);
		usernamePanel.SetHeaderOff();
		usernamePanel.setPreferredSize(new Dimension(200,600));
	    setLayout(new BorderLayout());
	    
	    add(scroll, BorderLayout.CENTER);
	    add(southPanel, BorderLayout.SOUTH);
	    add(usernamePanel, BorderLayout.EAST);
		
	    enterChatButton.addActionListener(new ActionListener()
		{
	    	@Override
	    	public void actionPerformed(ActionEvent actionEvent)
	    	{
	    		SendChat();
	    	}
		});
	    chatField.addActionListener(new ActionListener()
	    {
	    	@Override
	    	public void actionPerformed(ActionEvent actionEvent)
	    	{
	    		SendChat();
	    	}
	    });
	    
	}
	private void SendChat()
	{
		String user = ClientPanel.GetClientPanel().GetLoginPanel().GetUsername();
		textBox.append(user + ": " + chatField.getText().trim() + "\n");
		textBox.revalidate();
		ClientPanel.GetClientPanel().GetClient().SendObjectToServer(new ChatMessage(ParseDatabase.GetUsername(), "", chatField.getText()));
		chatField.setText("");
		chatField.revalidate();
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
			}
		});
	}
	public void AddUserToUsernamePanel(String username)
	{
		if(!usernamePanel.ElementExist(username, 0))
		{
			usernamePanel.AddElement(new Object[] { username });			
		}
	}
	
	public void RemoveUserFromUsernamePanel(String username)
	{
		if(usernamePanel.ElementExist(username, 0))
		{
			usernamePanel.RemoveElement(username, 0);			
		}
	}

	public void AddMsg(String msg)
	{
		textBox.append(msg.trim() + "\n");
		textBox.revalidate();
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
			}
		});
	}
	public void ClearField()
	{
		textBox.setText(null);
		chatField.setText(null);
		usernamePanel.ClearTable();
	}
}