package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

import database.ParseDatabase;
import messages.ChatMessage;
import messages.ConnectionMessage;
import messages.LoginUserMessage;
import messages.QuestionMessage;
import messages.ScoreMessage;
import util.Debug;

/**
 * The SixDegreesClient is located on the client side and is responsible for communicating with the server
 * as well as receiving messages from the server. This class can send messages to the server as well as
 * receive a message from a server and respond appropriately. 
 * 
 * @author AaronCheney
 */
public class SixDegreesClient extends Thread
{
	private Socket m_socket;
	private ObjectOutputStream m_objectOutputStream;
	private ObjectInputStream m_objectInputStream;
	private String m_hostName;
	private int m_port;
	private boolean connected = false;
	private boolean m_connectionFailed = false;
	
	public boolean GetConnected(){ return connected;}
	public boolean DidConnectionFail() { return m_connectionFailed; }
	/**
	 * Creates a new client with the given host name and port number. 
	 * 
	 * @param hostName The name of the host that you are trying to connect to.
	 * @param port The port number. 
	 */
	public SixDegreesClient(String hostName, int port)
	{
		m_hostName = hostName;
		m_port = port;
	}
	
	/**
	 * Initializes the Socket, ObjectInputStream, and ObjectOutputStream, and while
	 * the client is running will continually read objects from the input stream and
	 * handle them as they arrive. 
	 */
	@Override
	public void run()
	{
		try
		{
			m_socket = new Socket(m_hostName, m_port);
			m_objectOutputStream = new ObjectOutputStream(m_socket.getOutputStream());
			m_objectInputStream = new ObjectInputStream(m_socket.getInputStream());
			connected = true;
			while(true)
			{
				Object object = m_objectInputStream.readObject();
				HandleIncomingObject(object);
			}
		}
		catch(ClassNotFoundException cnfe)
		{
			Debug.DebugLog(cnfe.getMessage());
		}
		catch(IOException ioe)
		{
			Debug.DebugLog(ioe.getMessage());
		}
		finally
		{
			Disconnect();
		}
	}
	
	/**
	 * Sends an object to the server. 
	 * 
	 * @param object The object to be sent.
	 */
	public void SendObjectToServer(Object object)
	{
		if(m_objectOutputStream == null) { return; }
		try 
		{
			m_objectOutputStream.writeObject(object);
			m_objectOutputStream.flush();
		} 
		catch (IOException ioe) 
		{
			Debug.DebugLog("IOE Catch in SendObjectToOtherClients in client");
			Debug.DebugLog(ioe.getMessage());
		}
	}
	
	/**
	 * Alerts the server when this client disconnects. 
	 */
	public void AlertServerOnDisconnect()
	{
		if(m_objectOutputStream != null)
		{
			try 
			{
				m_objectOutputStream.writeObject(null);
			} 
			catch (IOException e) 
			{
				Debug.DebugLog(e.getMessage());
			}
		}
	}
	
	/**
	 * Processes an incoming object. When the null object is passed, that means the server
	 * disconnected, otherwise the objects will be handled based on the kind of message that
	 * is sent.
	 * 
	 * @param object
	 */
	private void HandleIncomingObject(Object object)
	{
		if(object == null)
		{
			Disconnect();
			return;
		}
		
		if(object instanceof ChatMessage)
		{
			RespondToChatMessage((ChatMessage)object);
		}
		else if(object instanceof ScoreMessage)
		{
			RespondToScoreMessage((ScoreMessage)object);
		}
		else if(object instanceof QuestionMessage)
		{
			RespondToQuestionMessage((QuestionMessage)object);
		}
		else if(object instanceof LoginUserMessage)
		{
			RespondToLoginMessage((LoginUserMessage) object);
		}
		else if(object instanceof ConnectionMessage)
		{
			RespondToConnectionMessage((ConnectionMessage) object);
		}
	}
	
	/**
	 * Responds to a chat message sent from the server. 
	 * 
	 * @param chatMessage The chat message.
	 */
	private void RespondToChatMessage(ChatMessage chatMessage)
	{
		Debug.DebugLog("Received a chat message from " + chatMessage.GetUsername());
		Debug.DebugLog(chatMessage.GetChatMessage());
		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetChatPanel().AddMsg(chatMessage.GetUsername() + ": " + chatMessage.GetChatMessage());
	}
	
	/**
	 * Responds to a score message sent from the server. 
	 * 
	 * @param scoreMessage The score message. 
	 */
	private void RespondToScoreMessage(ScoreMessage scoreMessage)
	{
		Debug.DebugLog("Received a score message from " + scoreMessage.GetUsername());
		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetHighScorePanel().AddScoreToUser(scoreMessage.GetUsername());//, scoreMessage.GetScore());
	}
	
	/**
	 * Responds to a question message sent from the server. 
	 * 
	 * @param questionMessage The question message. 
	 */
	private void RespondToQuestionMessage(QuestionMessage questionMessage)
	{
		Debug.DebugLog("Received a question message from " + questionMessage.GetUsername());
	}
	/**
	 * Responds to a login message sent from the server. 
	 * 
	 * @param loginUserMessage The login message. 
	 */
	private void RespondToLoginMessage(LoginUserMessage loginMessage)
	{
		Debug.DebugLog("Received a login message from " + loginMessage.GetUsername());
		if(loginMessage.IsLoggingIn())
		{
			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetChatPanel().AddUserToUsernamePanel(loginMessage.GetUsername());			
		}
		else
		{
			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetChatPanel().RemoveUserFromUsernamePanel(loginMessage.GetUsername());
		}
	}
	private void RespondToConnectionMessage(ConnectionMessage connectionMessage)
	{
		Debug.DebugLog("Received a connection message from " + connectionMessage.GetUsername());
		ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().ClearTable();
		Vector<String> connections = new Vector<String>();
		connections = ParseDatabase.GetAllConnections();
		
		for(String c : connections)
		{
			ClientPanel.GetClientPanel().GetRegUserGamePanel().GetConnectionPanel().AddElement(new Object[] {c});
		}
	}
	/**
	 * Disconnects all of the streams and the socket, and also alerts the server when the client
	 * has disconnected. 
	 */
	public void Disconnect()
	{
		try
		{
			if(m_objectOutputStream != null)
			{
				AlertServerOnDisconnect();
				m_objectOutputStream.close();
			}
			if(m_objectInputStream != null)
			{
				m_objectInputStream.close();
			}
			if(m_socket != null && !m_socket.isClosed())
			{
				m_socket.close();
			}
			connected = false;
		}
		catch(IOException ioe)
		{
			Debug.DebugLog("IOE Catch inside disconnect on client side.");
			Debug.DebugLog(ioe.getMessage());
		}
		finally
		{
			m_connectionFailed = true;
		}
	}
}
