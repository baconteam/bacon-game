package client;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.Button;
import gui.GUILayout;
import gui.IGUI;
import gui.InputField;
import gui.Panel;
import gui.PasswordInput;

public class LoginPanel extends Panel implements IGUI {

	private static final long serialVersionUID = 1L;

	private JButton loginButton, createNewButton, backButton;
	private InputField userNameTF;
	private PasswordInput passwordTF;
	private JLabel welcomeLabel;
	private JPanel loginPanel;
	private JPanel passwordPanel;
	private JPanel buttonPanel;
	private GridBagConstraints gbc;
	
	private final int LOGIN_ACTION = 0;
	private final int REGISTER_ACTION = 1;
	private final int BACK_ACTION = 2;

	public LoginPanel(ActionListener[] actionListeners, ImageIcon inImage) {
		super(actionListeners, inImage);
	}
	public String GetUsername()
	{ 
		return userNameTF.GetInput();
	}
	public String GetPassword()
	{
		return passwordTF.GetPassword();
	}
	@Override
	public void InitializeComponents() {
		welcomeLabel = new JLabel("Welcome to the USC Game!");
		userNameTF = new InputField("Username: ", GUILayout.Vertical, 25, 30);
		passwordTF = new PasswordInput("Password:", GUILayout.Vertical, 25, 30);
		loginButton = new Button("Login", Constants.Header2Size);
		createNewButton = new Button("Register", Constants.Header2Size);
		backButton = new Button("Back", Constants.Header2Size);
		loginPanel = new JPanel();
		passwordPanel = new JPanel();
		buttonPanel = new JPanel();
		gbc = new GridBagConstraints();
		welcomeLabel.setFont(Constants.TitleFont);
		welcomeLabel.setForeground(Constants.blue);
	}

	@Override
	public void CreateGUI() {
		setLocation(100,100);
		setSize(500,600);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		loginPanel.setLayout(new BorderLayout());
		passwordPanel.setLayout(new BorderLayout());
		buttonPanel.setLayout(new GridBagLayout());
		add(welcomeLabel);
		
		loginPanel.add(userNameTF, BorderLayout.CENTER);	
		loginPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		add(loginPanel);
		
		passwordPanel.add(passwordTF, BorderLayout.CENTER);		
		passwordPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		add(passwordPanel);
		
		gbc.gridwidth = 1;
        gbc.ipadx = 100;
        gbc.gridy = 1;
        gbc.ipady = 30;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0,23,20,20);
        buttonPanel.add(loginButton,gbc);
        gbc.gridy = 2;
        gbc.insets = new Insets(0,20,20,20);
        buttonPanel.add(createNewButton,gbc);
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 20, 20, 20);
        buttonPanel.add(backButton, gbc);
        buttonPanel.setBorder(new EmptyBorder(30, 30, 30, 30));
        add(buttonPanel);
        setBorder(new EmptyBorder(70, 70, 70, 70));
	}

	@Override
	public void AddEvents() {
		loginButton.addActionListener(GetActionListener(LOGIN_ACTION));
		createNewButton.addActionListener(GetActionListener(REGISTER_ACTION));
		backButton.addActionListener(GetActionListener(BACK_ACTION));
	}
	public void ClearField(){
		userNameTF.RemoveText();
		passwordTF.RemoveText();
	}
	public void ClearUserField() {
		userNameTF.RemoveText();
	}
	public void ClearPassField() {
		passwordTF.RemoveText();
	}
}
