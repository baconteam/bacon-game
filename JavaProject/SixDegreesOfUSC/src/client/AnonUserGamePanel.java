package client;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class AnonUserGamePanel extends RegUserGamePanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GuestMarkerPanel m_guest;
	private JPanel m_rightPanel;
	public AnonUserGamePanel(ImageIcon inImage) {
		super(inImage);
	}
	
	@Override
	public void InitializeComponents() {
		super.InitializeComponents();
		m_guest = new GuestMarkerPanel(null);
		m_rightPanel = new JPanel();
	}
	
	@Override
	public void CreateGUI() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        m_topMidRightPanel.setLayout(new BoxLayout(m_topMidRightPanel, BoxLayout.Y_AXIS));
        m_topMidRightPanel.add(m_highscorePanel);
      	
        m_rightPanel.setLayout(new BoxLayout(m_rightPanel, BoxLayout.Y_AXIS));
        m_rightPanel.add(m_userPanel);
        m_rightPanel.add(m_guest);
        
        m_topRightPanel.setLayout(new BoxLayout(m_topRightPanel, BoxLayout.X_AXIS));
        m_topRightPanel.add(m_topMidRightPanel);
        m_topRightPanel.add(m_rightPanel);
        m_topRightPanel.setMinimumSize(new Dimension(150,500));
      	m_topRightPanel.setPreferredSize(new Dimension(150,500));
      	
        m_topPanel.setLayout(new BoxLayout(m_topPanel, BoxLayout.X_AXIS));
        m_topPanel.add(m_gamePanel);
        m_topPanel.add(m_topRightPanel);
        m_topPanel.setMinimumSize(new Dimension(300,500));
      	m_topPanel.setPreferredSize(new Dimension(400,500));
        
        add(m_topPanel);
	}
	
	@Override
	public void AddEvents() {
	}

}
