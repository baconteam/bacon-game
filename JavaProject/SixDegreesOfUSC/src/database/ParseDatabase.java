package database;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import almonds.LogInCallback;
import almonds.Parse;
import almonds.ParseException;
import almonds.ParseObject;
import almonds.ParseQuery;
import almonds.ParseUser;
import almonds.SignupCallback;

public class ParseDatabase {
	private static ParseObject currentUser = null;
	private static boolean wasLegal = true;

	private ParseDatabase(){	}
	public static void main(String[] args){
		Init();
		Login("briantotheyanyan", "YanYan");
		Vector<String> connections = ParseDatabase.GetAllConnections();
		for(int i = 0; i < connections.size(); ++i){
			System.out.println(connections.get(i));
		}
		/*
		//AddUser("Schenel", "Sexy time", "Stephen");
		Login("Schenel", "Sexy time");
		//AddUser("briantotheyanyan", "YanYan", "Brian");
		//Login("briantotheyanyan", "YanYan");
		AddClass("EE352", 20151);
		incrementScore();
		Login("briantotheyanyan", "YanYan");
		AddClass("CSCI360", 20153);
		System.out.println("Done");
		 */
	}
	public static String GetUsername(){
		if(currentUser == null){
			return null;
		}
		return currentUser.get("Username").toString();
	}

	public static void Logout(){
		currentUser = null;
	}

	public static String GetName(){
		if(currentUser == null){
			return null;
		}
		return currentUser.get("Name").toString();
	}

	public static void Init(){
		Parse.initialize("lKzJw9rHcdJL1VWX5x201skg8cBxsknx1v8JrAEi", "jw7uk6A1PGiKFadRAs1jFUiYQhEGefbI3SZUPHdr");
	}


	public static Vector<String> GetAllUsernamesFromClass(String className){
		Vector<String> answer = new Vector<String>();
		ParseQuery query = new ParseQuery("ClassToUser");
		query.whereEqualTo("Course", className);
		try{
			List<ParseObject> courses = query.find();
			if(courses.size() > 0){
				String[] toParse = courses.get(0).get("Students").toString().split(",");
				for(int i = 0; i < toParse.length; ++i){
					answer.add(toParse[i]);
				}
			}
		}
		catch(ParseException e){
			System.out.println(e.getLocalizedMessage());
		}
		return answer;
	}

	public static HashMap<String, Vector<String> > GetAllClassesUser(){
		HashMap<String, Vector<String> > answer = new HashMap<String, Vector<String>>();

		Vector<ParseObject> allUsers = GetAllClasses();
		for(int i = 0; i < allUsers.size(); i++){
			ParseObject currentObject = allUsers.get(i);
			Vector<String> currentSchedule = new Vector<String>();
			String[] currentParseSchedule = currentObject.get("Students").toString().split(",");
			for(int j = 0; j < currentParseSchedule.length; ++j){
				currentSchedule.add(currentParseSchedule[j]);
			}
			answer.put(currentObject.get("Course").toString(), currentSchedule);
		}
		return answer;
	}

	public static Vector<String> GetAllUsernamesFromClass(String className, int semester){
		return GetAllUsernamesFromClass(className + "-" + semester);
	}

	public static boolean AddUser(String username, String password, String name){
		ParseUser user = new ParseUser();
		user.setUsername(username);
		user.setPassword(password);
		user.put("Name", name);
		user.put("Classes", "");
		user.signUpInBackground(new SignupCallback(){
			@Override
			public void done(ParseException e) {
				if(e == null){
					ParseObject newUser = new ParseObject("UserToClass");
					newUser.put("Username", username);
					newUser.put("Name", name);
					newUser.put("Classes", "");
					newUser.put("Semesters", "");
					newUser.put("Score", 0);
					newUser.saveInBackground();
					currentUser = newUser;
					wasLegal = true;
				}
				else{
					System.out.println(e.getCode());
					System.out.println(e.getMessage());
					wasLegal = false;
				}
			}
		});
		return wasLegal;
	}

	/* Vector of Vectors to get Username, Name and Score.
	 * The Inner vector has index 0 being username
	 * index 1 being name
	 * index 2 being score
	 */
	public static Vector<Vector<String> > GetUsersSortedByScore(){
		Vector<Vector<String> > answer = new Vector<Vector<String> >();
		Vector<ParseObject> users = GetAllUsersSortedDescending("Score");
		for(int i = 0; i < users.size(); ++i){
			Vector<String> current = new Vector<String>();
			ParseObject currentUser = users.get(i);
			current.add(currentUser.get("Username").toString());
			current.add(currentUser.get("Name").toString());
			current.add("" + (int)currentUser.get("Score"));
			answer.add(current);
		}
		return answer;
	}

	public static HashMap<String, Vector<String>> GetAllUserClasses(){
		HashMap<String, Vector<String> > answer = new HashMap<String, Vector<String>>();
		Vector<ParseObject> allUsers = GetAllUsers();
		for(int i = 0; i < allUsers.size(); i++){
			ParseObject currentObject = allUsers.get(i);
			Vector<String> currentSchedule = new Vector<String>();
			String[] currentParseSchedule = currentObject.get("Classes").toString().split(",");
			for(int j = 0; j < currentParseSchedule.length; ++j){
				currentSchedule.add(currentParseSchedule[j]);
			}
			answer.put(currentObject.get("Username").toString(), currentSchedule);
		}
		return answer;
	}

	public static Vector<ParseObject> GetAllUsers(){
		ParseQuery query = new ParseQuery("UserToClass");
		Vector<ParseObject> AllUsers = new Vector<ParseObject>();
		List<ParseObject> allObjects;
		try{
			allObjects = query.find();
			System.out.println(allObjects.size());
		}catch(ParseException e){
			allObjects = new Vector<ParseObject>();
			System.out.println("Could not contact Parse");
		}
		for(int i = 0; i < allObjects.size(); ++i){
			AllUsers.add(allObjects.get(i));
		}
		return AllUsers;
	}

	public static Vector<ParseObject> GetAllClasses(){
		ParseQuery query = new ParseQuery("ClassToUser");
		Vector<ParseObject> AllUsers = new Vector<ParseObject>();
		List<ParseObject> allObjects;
		try{
			allObjects = query.find();
			System.out.println(allObjects.size());
		}catch(ParseException e){
			allObjects = new Vector<ParseObject>();
			System.out.println("Could not contact Parse");
		}
		for(int i = 0; i < allObjects.size(); ++i){
			AllUsers.add(allObjects.get(i));
		}
		return AllUsers;
	}

	public static Vector<ParseObject> GetAllUsersSortedDescending(String key){
		ParseQuery query = new ParseQuery("UserToClass");
		query.addDescendingOrder(key);
		Vector<ParseObject> AllUsers = new Vector<ParseObject>();
		List<ParseObject> allObjects;
		try{
			allObjects = query.find();
			System.out.println(allObjects.size());
		}catch(ParseException e){
			allObjects = new Vector<ParseObject>();
			System.out.println("Could not contact Parse");
		}
		for(int i = 0; i < allObjects.size(); ++i){
			AllUsers.add(allObjects.get(i));
		}
		return AllUsers;
	}

	public static Vector<ParseObject> GetAllUsersSortedAscending(String key){
		ParseQuery query = new ParseQuery("UserToClass");
		query.addAscendingOrder(key);
		Vector<ParseObject> AllUsers = new Vector<ParseObject>();
		List<ParseObject> allObjects;
		try{
			allObjects = query.find();
			System.out.println(allObjects.size());
		}catch(ParseException e){
			allObjects = new Vector<ParseObject>();
			System.out.println("Could not contact Parse");
		}
		for(int i = 0; i < allObjects.size(); ++i){
			AllUsers.add(allObjects.get(i));
		}
		return AllUsers;
	}

	public static boolean Login(String username, String password){
		ParseUser.logIn(username, password, new LogInCallback(){

			@Override
			public void done(ParseUser obj, ParseException e) {
				if(e == null){
					ParseQuery query = new ParseQuery("UserToClass");
					query.whereEqualTo("Username", username);
					try{
						currentUser = query.find().get(0);
						wasLegal = true;
					}
					catch(ParseException pe){
						System.out.println(pe.getLocalizedMessage());
						wasLegal = false;
					}
				}
				else{
					System.out.println("Not Found");
					System.out.println(e.getMessage());
					wasLegal = false;
				}
			}

		});
		return wasLegal;
	}

	public static Object GetFromUser(String key){
		if(currentUser == null){
			return null;
		}
		if(currentUser.containsKey(key)){
			return currentUser.get("Name");
		}
		return null;
	}

	public static boolean incrementScore(){
		int score = (int)currentUser.get("Score");
		score++;
		currentUser.put("Score", score);
		try{
			currentUser.save();
			return true;
		}
		catch(ParseException e){
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	public static boolean decrementScore(){
		int score = (int)currentUser.get("Score");
		score--;
		currentUser.put("Score", score);
		try{
			currentUser.save();
			return true;
		}
		catch(ParseException e){
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}	
	
	public static boolean AddClass(String addClass, int semester, ParseObject currentUser){
		if(currentUser != null){
			String[] currentUserClasses = currentUser.get("Classes").toString().split(",");
			Vector<String> classVector = new Vector<String>();
			for(int i = 0; i < currentUserClasses.length; ++i){
				classVector.add(currentUserClasses[i]);
			}
			for(int i = 0; i < classVector.size(); ++i){
				if(classVector.get(i).equals(addClass + "-" + semester)){
					System.out.println("Hmm");
					return false;
				}
			}
			String currentClasses = currentUser.get("Classes").toString();
			currentClasses += addClass + "-" + semester + ",";
			currentUser.put("Classes", currentClasses);
			try{
				currentUser.save();
				ParseQuery classesQuery = new ParseQuery("ClassToUser");
				classesQuery.whereEqualTo("Course", addClass + "-" + semester);
				List<ParseObject> classToUser = classesQuery.find();
				if(classToUser.size() > 0){
					ParseObject newClassToStudent = classToUser.get(0);
					String studentList = newClassToStudent.get("Students").toString();
					studentList += currentUser.get("Username").toString() + ",";
					newClassToStudent.put("Students", studentList);
					newClassToStudent.save();
				}
				else{
					ParseObject newClassToStudent = new ParseObject("ClassToUser");
					newClassToStudent.put("Course", addClass + "-" + semester);
					newClassToStudent.put("Students", currentUser.get("Username").toString() + ",");
					newClassToStudent.save();
				}
				return true;
			}
			catch(ParseException e){
				System.out.println(e.getLocalizedMessage());
				System.out.println("HerE?");
				return false;
			}
		}
		return false;
	}

	public static boolean AddClass(String addClass, int semester){
		ParseQuery millerFinder = new ParseQuery("UserToClass");
		millerFinder.whereEqualTo("Username", "JeffreyMiller");
		ParseObject miller;
		boolean isMiller = false;
		try{
			miller = millerFinder.find().get(0);
			if(AddClass(addClass, semester, miller) && miller.hasSameId(currentUser)){
				isMiller = true;
			}
		}
		catch(Exception e){
			System.out.println(e.getLocalizedMessage());
		}
		return isMiller || AddClass(addClass, semester, currentUser);
	}

	public static Vector<String> GetAllConnections(){
		Vector<String> connections = new Vector<String>();
		HashMap<String, Vector<String>> AllUserClasses = GetAllUserClasses();
		HashMap<String, Vector<String>> AllClassesUser = GetAllClassesUser();
		Vector<String> classes = null;
		if(currentUser != null && currentUser.get("Username") != null){
			classes = AllUserClasses.get(currentUser.get("Username").toString());
		}
		else{
			return connections;
		}
		if(classes == null)
		{
			return connections;
		}
		for(int i = 0; i < classes.size(); ++i){
			Vector<String> studentsInClass = AllClassesUser.get(classes.get(i));
			if(studentsInClass == null)
			{
				continue;
			}
			for(int j = 0; j < studentsInClass.size(); ++j){
				if(!connections.contains(studentsInClass.get(j)) && !studentsInClass.get(j).equals(ParseDatabase.GetUsername())){
					connections.add(studentsInClass.get(j));
				}
			}
		}
		
		Vector<String> SecondDegreeConnections = new Vector<String>();
		
		for(int k = 0; k < connections.size(); ++k){
			classes = AllUserClasses.get(connections.get(k));
			if(classes == null)
			{
				continue;
			}
			for(int i = 0; i < classes.size(); ++i){
				Vector<String> studentsInClass = AllClassesUser.get(classes.get(i));
				if(studentsInClass == null)
				{
					continue;
				}
				for(int j = 0; j < studentsInClass.size(); ++j){
					if(!connections.contains(studentsInClass.get(j)) && !studentsInClass.get(j).equals(ParseDatabase.GetUsername())){
						connections.add(studentsInClass.get(j));
						SecondDegreeConnections.add(studentsInClass.get(j));
					}
				}
			}
		}
		
		for(int k = 0; k < SecondDegreeConnections.size(); ++k){
			classes = AllUserClasses.get(SecondDegreeConnections.get(k));
			if(classes == null)
			{
				continue;
			}
			for(int i = 0; i < classes.size(); ++i){
				Vector<String> studentsInClass = AllClassesUser.get(classes.get(i));
				if(studentsInClass == null)
				{
					continue;
				}
				for(int j = 0; j < studentsInClass.size(); ++j){
					if(!connections.contains(studentsInClass.get(j)) && !studentsInClass.get(j).equals(ParseDatabase.GetUsername())){
						connections.add(studentsInClass.get(j));
						SecondDegreeConnections.add(studentsInClass.get(j));
					}
				}
			}
		}
		
		return connections;
	}

	public static Vector<String> GetClassesForCurrentUser(){
		Vector<String> returnClasses = new Vector<String>();
		if(currentUser != null){
			String[] classes = currentUser.get("Classes").toString().split(",");
			for(int i = 0; i < classes.length; ++i){
				returnClasses.add(classes[i]);
			}
		}
		return returnClasses;
	}
}
