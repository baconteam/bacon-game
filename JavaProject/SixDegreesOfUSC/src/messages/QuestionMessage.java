package messages;

/**
 * A NetworkMessage that encapsulates information when a user submits their response to a question
 * in our mini-game.
 * 
 * @author AaronCheney
 */
public class QuestionMessage extends NetworkMessage
{
	private static final long serialVersionUID = 1L;
	
	private boolean m_correct;
	
	/**
	 * 
	 * @param username The user name of the sender.
	 * @param statusMessage The status message associated with this NetworkMessage.
	 * @param correct A boolean value indicating whether or not a player answers a question correctly. 
	 */
	public QuestionMessage(String username, String statusMessage, boolean correct)
	{
		super(username, statusMessage);
		
		m_correct = correct;
	}
	
	/**
	 * 
	 * @return A boolean value indicating whether or not the user answered the question correctly. 
	 */
	public boolean IsCorrect() { return m_correct; }
}
