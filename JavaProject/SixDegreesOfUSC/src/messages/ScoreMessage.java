package messages;

/**
 * A NetworkMessage that encapsulates information regarding a player's score when they answer a question. 
 * 
 * @author AaronCheney
 */
public class ScoreMessage extends NetworkMessage
{
	private static final long serialVersionUID = 1L;
	
	//private int m_score;
	
	/**
	 * 
	 * @param username The user name of the sender.
	 * @param statusMessage The status message associated with this NetworkMessage.
	 * @param score The score the player earned.
	 */
	public ScoreMessage(String username, String statusMessage)//, int score)
	{
		super(username, statusMessage);
		
		//m_score = score;
	}
	
	/**
	 * 
	 * @return The score the player earned.
	 */
	//public int GetScore() { return m_score; }
}
