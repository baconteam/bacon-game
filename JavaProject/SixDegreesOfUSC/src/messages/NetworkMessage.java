package messages;

import java.io.Serializable;

/**
 * The NetworkMessage class is the base class for all messages that are sent across the network. It 
 * stores the user name for the client that sends the message as well as a status message that can 
 * indicate information about the message. 
 * 
 * @author AaronCheney
 */
public abstract class NetworkMessage implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String m_username, m_statusMessage;
	
	/**
	 * 
	 * @param username The user name of the sender. 
	 * @param statusMessage The status message associated with this NetworkMessage
	 */
	public NetworkMessage(String username, String statusMessage)
	{
		m_username = username;
		m_statusMessage = statusMessage;
	}
	
	/**
	 * 
	 * @return The status message for this NetworkMessage
	 */
	public String GetMessage() { return m_statusMessage; }
	
	/**
	 * 
	 * @return The user name that sent the message.
	 */
	public String GetUsername() { return m_username; }
}
