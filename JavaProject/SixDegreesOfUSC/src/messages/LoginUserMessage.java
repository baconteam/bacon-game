package messages;

public class LoginUserMessage extends NetworkMessage {

	private static final long serialVersionUID = 1L;

	private boolean m_login;
	
	/**
	 * 
	 * @param username The user name of the sender.
	 * @param statusMessage Any status message that should be attached to this NetworkMessage
	 * @param login The user is logged in
	 */
	public LoginUserMessage(String username, String statusMessage, boolean IsLogin) {
		super(username, statusMessage);
		
		m_login = IsLogin;
	}
	/**
	 * 
	 * @return If the user is currently login to the game
	 */
	public boolean IsLoggingIn() { return m_login; }
}
