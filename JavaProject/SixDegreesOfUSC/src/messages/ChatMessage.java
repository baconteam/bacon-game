package messages;

/**
 * A NetworkMessage that encapsulates a chat message sent from a user.
 * 
 * @author AaronCheney
 */
public class ChatMessage extends NetworkMessage
{
	private static final long serialVersionUID = 1L;
	
	private String m_chatMessage;
	
	/**
	 * 
	 * @param username The user name of the sender.
	 * @param statusMessage Any status message that should be attached to this NetworkMessage
	 * @param chatMessage The chat message sent by the user.
	 */
	public ChatMessage(String username, String statusMessage, String chatMessage)
	{
		super(username, statusMessage);
		
		m_chatMessage = chatMessage;
	}
	
	/**
	 * 
	 * @return The chat message sent by the user.
	 */
	public String GetChatMessage() { return m_chatMessage; }
}
