package util;

import server.SixDegreesServerPanel;

public class Debug 
{
	private static boolean m_debugActive = true;
	
	public static void DebugLog(String message)
	{
		if(m_debugActive)
		{
			System.out.println(message);
			SixDegreesServerPanel.AddMessage(message);
		}
	}
}
