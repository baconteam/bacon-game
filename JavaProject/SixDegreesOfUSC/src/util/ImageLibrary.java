package util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ImageLibrary 
{
	private static Map<String, ImageIcon> m_imageMap;
	
	public ImageLibrary()
	{
		m_imageMap = new HashMap<String, ImageIcon>();
	}
	
	public static ImageIcon GetImage(String fileName)
	{
		if(m_imageMap.containsKey(fileName))
		{
			return m_imageMap.get(fileName);
		}
		else
		{
			ImageIcon image = null;
			try
			{
				image = new ImageIcon(ImageIO.read(new File(fileName)));
				m_imageMap.put(fileName, image);
			}
			catch(IOException ioe)
			{
				Debug.DebugLog(ioe.getMessage());
			}
			return image;
		}
	}
}
