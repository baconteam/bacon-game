package server;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import gui.Button;
import gui.GUILayout;
import gui.IGUI;
import gui.InputField;
import gui.Panel;
import gui.TextArea;
import gui.Title;
import util.Debug;

public class SixDegreesServerPanel extends Panel implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private SixDegreesServer m_server;
	private JPanel m_buttonPanel;
	private static TextArea m_textArea;
	private Title m_title;
	private InputField m_inputField;
	private Button m_startButton;
	private Button m_disconnectButton;
	private boolean m_isConnected;
	
	public SixDegreesServerPanel(ImageIcon inImage, boolean inDrawBack) 
	{
		super(inImage, inDrawBack);
		m_isConnected = false;
	}
	
	public static void AddMessage(String message)
	{
		if(m_textArea != null)
		{
			m_textArea.AddText(message);
		}
	}
	
	public void Disconnect()
	{
		if(m_server != null)
		{
			m_server.AlertAllClientsOnDisconnect();
		}
	}
	
	@Override
	public void InitializeComponents() 
	{
		m_title = new Title(null, "Six Degrees Server", Constants.Header1Size);
		m_inputField = new InputField("Port:", GUILayout.Vertical, Constants.Header2Size, 30);
		m_startButton = new Button("Start", Constants.Header2Size);
		m_disconnectButton = new Button("Disconnect", Constants.Header2Size);
		m_buttonPanel = new JPanel(new GridBagLayout());
		m_textArea = new TextArea(Constants.Header2Size);
	}

	@Override
	public void CreateGUI() 
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		m_title.setBorder(new EmptyBorder(30, 30, 30, 30));
		m_inputField.setBorder(new EmptyBorder(30, 30, 30, 30));
		
		m_startButton.setSize(100, 30);
		m_disconnectButton.setSize(100, 30);
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.ipadx = 100;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.ipady = 30;
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.insets = new Insets(40,40,40,40);
		m_buttonPanel.add(m_startButton, gridBagConstraints);
		gridBagConstraints.gridx = 1;
		m_buttonPanel.add(m_disconnectButton, gridBagConstraints);
		
		add(m_title);
		add(m_inputField);
		add(m_buttonPanel);
		add(Box.createVerticalGlue());
		
		JScrollPane scrollPane = new JScrollPane(m_textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane);

		setSize(600, 400);
		setLocation(0, 0);
		setVisible(true);
	}

	@Override
	public void AddEvents() 
	{		
		m_startButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent actionEvent)
			{
				if(!m_isConnected)
				{
					try
					{
						int portNumber = Integer.parseInt(m_inputField.GetInput());
						m_server = new SixDegreesServer(portNumber);
						m_server.start();
						m_isConnected = true;
					}
					catch(NumberFormatException nfe)
					{
						Debug.DebugLog("Invalid port number format");
						Debug.DebugLog(nfe.getMessage());
					}
				}
			}
		});
		
		m_disconnectButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent actionEvent)
			{
				if(m_isConnected)
				{
					m_server.AlertAllClientsOnDisconnect();
					m_server = null;
					m_isConnected = false;
				}
			}
		});
	}
}
