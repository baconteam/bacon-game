package server;

import java.awt.Font;

public class Constants 
{
	public static Font BaseFont = new Font("Times New Roman", Font.PLAIN, 12);
	
	public static int Header1Size = 36;
	public static int Header2Size = 24;
	public static int Header3Size = 16;
	public static int Header4Size = 12;
}
