package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import util.Debug;

/**
 * The SixDegreesServerClientConnection is the intermediate Thread between the server and the client, and is
 * maintained on the server side.
 * 
 * @author AaronCheney
 */
public class SixDegreesServerClientConnection extends Thread
{
	private SixDegreesServer m_server;
	private Socket m_socket;
	private ObjectOutputStream m_objectOutputStream;
	private ObjectInputStream m_objectInputStream;
	private boolean m_hasInitialized = false;
	
	/**
	 * Creates a new communication thread with a client so that communication can be sent back
	 * and forth between the server and the client while the server listens for new connections. 
	 * 
	 * @param server A reference to the server. 
	 * @param socket The socket.
	 */
	public SixDegreesServerClientConnection(SixDegreesServer server, Socket socket)
	{
		m_server = server;
		m_socket = socket;
	}
	
	/**
	 * Alerts the client when the server has disconnected by writing null to the stream. 
	 */
	public void AlertOnServerDisconnect()
	{
		try
		{
			if(m_objectOutputStream != null)
			{
				m_objectOutputStream.writeObject(null);
				m_objectOutputStream.flush();
				m_objectOutputStream.close();
			}
			if(m_objectInputStream != null)
			{
				m_objectInputStream.close();
			}
			if(m_socket != null && !m_socket.isClosed())
			{
				m_socket.close();
			}
		}
		catch(IOException ioe)
		{
			Debug.DebugLog("ServerClientConnection IOE exception on server disconnect");
			Debug.DebugLog(ioe.getMessage());
		}
	}
	
	/**
	 * Receives an object from the server. 
	 * 
	 * @param object The received object. 
	 */
	public void ReceiveObjectFromServer(Object object)
	{
		if(m_objectOutputStream != null)
		{
			try 
			{
				m_objectOutputStream.writeObject(object);
				m_objectOutputStream.flush();
			} 
			catch (IOException ioe) 
			{
				Debug.DebugLog("IOE Catch in ServerClientConnection ReceiveObjectFromServer method");
				Debug.DebugLog(ioe.getMessage());
			}
		}
	}
	
	/**
	 * Initializes the streams and continually reads objects from the input stream. When null is read
	 * in, the finally block is triggered which then performs all of the necessary disconnect operations.
	 */
	@Override
	public void run()
	{
		try
		{
			m_objectOutputStream = new ObjectOutputStream(m_socket.getOutputStream());
			m_objectInputStream = new ObjectInputStream(m_socket.getInputStream());
			m_hasInitialized = true;
			while(true)
			{
				Object object = m_objectInputStream.readObject();
				if(object == null) 
				{ 
					m_server.RemoveClientOnDisconnect(this);
					break; 
				}
				m_server.ReceiveObjectFromClient(this, object);
			}
		}
		catch(ClassNotFoundException cnfe)
		{
			Debug.DebugLog("ServerClientConnection CNFE Catch in run method");
			Debug.DebugLog(cnfe.getMessage());
		}
		catch(IOException ioe)
		{
			Debug.DebugLog("ServerClientConnection IOE Catch in run method");
			Debug.DebugLog(ioe.getMessage());
		}
		finally
		{
			try
			{
				if(m_objectOutputStream != null)
				{
					m_objectOutputStream.close();
				}
				if(m_objectInputStream != null)
				{
					m_objectInputStream.close();
				}
				if(m_socket != null && !m_socket.isClosed())
				{
					m_socket.close();
				}
			}
			catch(IOException ioe)
			{
				Debug.DebugLog("In finally block for ServerClientConnection run method");
				Debug.DebugLog(ioe.getMessage());
			}
		}
	}
	
	/**
	 * Returns true when the client has initialized all of the streams. 
	 * 
	 * @return Whether or not the the client has initialized all of the streams.
	 */
	public boolean HasInitialized() { return m_hasInitialized; }
}
