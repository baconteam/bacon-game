/**
 * Team 25: Six Degrees of USC:
 * Team Members:
 * Aaron Cheney
 * Erica Engle
 * Nathan Chau
 * Shin 
 * Brian Yan
 */

package server;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import gui.IGUI;

public class SixDegreesServerGUI extends JFrame implements IGUI
{
	private static final long serialVersionUID = 1L;
	
	private SixDegreesServerPanel m_serverPanel;
	
	public static void main(String[] args)
	{
		new SixDegreesServerGUI("Six Degrees Server");
	}
	
	public SixDegreesServerGUI(String windowTitle)
	{
		super(windowTitle);
		
		InitializeComponents();
		CreateGUI();
		AddEvents();
	}

	@Override
	public void InitializeComponents() 
	{
		m_serverPanel = new SixDegreesServerPanel(null, false);
	}

	@Override
	public void CreateGUI() 
	{
		add(m_serverPanel);
		setSize(600, 400);
		setVisible(true);
		pack();
	}

	@Override
	public void AddEvents() 
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent windowEvent)
			{
				m_serverPanel.Disconnect();
			}
		});
	}
}
