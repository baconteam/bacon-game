package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import messages.ChatMessage;
import messages.ConnectionMessage;
import messages.LoginUserMessage;
import messages.QuestionMessage;
import messages.ScoreMessage;
import util.Debug;

/**
 * The SixDegreesServer is responsible for tracking all communication between the clients and the server. For each
 * new client that connects, an new SixDegreesServerClientConnection Thread is started for communicating. Based
 * on incoming messages from the clients the server handles them differently. 
 * 
 * @author AaronCheney
 */
public class SixDegreesServer extends Thread
{
	private ServerSocket m_serverSocket = null;
	private Vector<SixDegreesServerClientConnection> m_connectedClients;
	private Vector<Object> m_previouslySentObjects;
	private final int m_port;
	private boolean m_acceptingConnections;
	
	{
		m_connectedClients = new Vector<SixDegreesServerClientConnection>();
		m_previouslySentObjects = new Vector<Object>();
		
	}
	
	/**
	 * Initializes a server on the given port. 
	 * 
	 * @param port The port number on which the server will be listening.
	 */
	public SixDegreesServer(int port)
	{
		m_port = port;
		m_acceptingConnections = true;
	}
	
	/**
	 * Calls AlertOnServerDisconnect() on all clients when the server disconnects so they can 
	 * close streams and return to the main screen. 
	 */
	public void AlertAllClientsOnDisconnect()
	{
		for(SixDegreesServerClientConnection client : m_connectedClients)
		{
			client.AlertOnServerDisconnect();
		}
		m_acceptingConnections = false;
	}
	
	/**
	 * Receives an object from a client and processes the object. 
	 * 
	 * @param sendingClient The client that is sending the object.
	 * @param object The object being sent by the client.
	 */
	public void ReceiveObjectFromClient(SixDegreesServerClientConnection sendingClient, Object object)
	{
		HandleIncomingObject(sendingClient, object);
	}
	
	/**
	 * Sends an object to all clients except the client that sent it originally. 
	 * 
	 * @param sendingClient The client that sent the original message, so they don't receive the object twice.
	 * @param object The object that will be sent.
	 */
	public void BroadcastObjectToAllClients(SixDegreesServerClientConnection sendingClient, Object object)
	{		
		for(SixDegreesServerClientConnection client : m_connectedClients)
		{
			if(client != sendingClient)
			{
				client.ReceiveObjectFromServer(object);
			}
		}
	}
	
	/**
	 * Removes a client from the list of connected clients when they disconnect from the server. 
	 * 
	 * @param disconnectingClient The client that disconnected
	 */
	public void RemoveClientOnDisconnect(SixDegreesServerClientConnection disconnectingClient)
	{	
		if(m_connectedClients.remove(disconnectingClient))
		{
			Debug.DebugLog("Removed a connected client");
		}
	}
	
	/**
	 * Initializes the server on the given port and continually listens for connection requests from clients. 
	 */
	@Override
	public void run()
	{
		try
		{
			m_serverSocket = new ServerSocket(m_port);
			while(m_acceptingConnections)
			{
				AcceptConnection();
			}
		}
		catch(IOException ioe)
		{
			Debug.DebugLog("IOE Catch in Server run method.");
			Debug.DebugLog(ioe.getMessage());
		}
		finally
		{
			try
			{
				if(m_serverSocket != null)
				{
					AlertAllClientsOnDisconnect();
					m_serverSocket.close();
					m_serverSocket = null;
				}
			}
			catch(IOException ioe)
			{
				Debug.DebugLog("IOE Catch in Server when closing ServerSocket");
				Debug.DebugLog(ioe.getMessage());
			}
		}
	}
	
	/**
	 * Waits for a connection from the client and starts a new thread for communication with that
	 * client. The client is added to a list of connected clients and they are sent all of the 
	 * back-logged messages. 
	 * 
	 * @throws IOException
	 */
	private void AcceptConnection() throws IOException
	{
		Debug.DebugLog("Waiting for connection...");
		Socket socket = m_serverSocket.accept();
		SixDegreesServerClientConnection sixDegreesClient = new SixDegreesServerClientConnection(this, socket);
		sixDegreesClient.start();
		m_connectedClients.add(sixDegreesClient);
		
		while(!sixDegreesClient.HasInitialized())
		{
			try 
			{
				Thread.sleep(1);
			} 
			catch (InterruptedException ie) 
			{
				Debug.DebugLog("InterruptedException catch in AcceptConnection method for Server");
				Debug.DebugLog(ie.getMessage());
			}
			finally
			{
				if(sixDegreesClient == null) { return; }
				Debug.DebugLog("Waiting for client to initialize...");
			}
		}
		
		for(int i = 0; i < m_previouslySentObjects.size(); ++i)
		{
			sixDegreesClient.ReceiveObjectFromServer(m_previouslySentObjects.get(i));
		}
	}
	
	/**
	 * Responds to an incoming message from a client and decides what to do for each kind of message. 
	 * 
	 * @param sendingClient The client that sent the object.
	 * @param object The object that was sent.
	 */
	private void HandleIncomingObject(SixDegreesServerClientConnection sendingClient, Object object)
	{	
		if(object instanceof ChatMessage)
		{
			RespondToChatMessage(sendingClient, (ChatMessage)object);
		}
		else if(object instanceof ScoreMessage)
		{
			RespondToScoreMessage(sendingClient, (ScoreMessage)object);
		}
		else if(object instanceof QuestionMessage)
		{
			RespondToQuestionMessage(sendingClient, (QuestionMessage)object);
		}
		else if(object instanceof LoginUserMessage)
		{
			RespondToLoginMessage(sendingClient, (LoginUserMessage)object);
		}
		else if(object instanceof ConnectionMessage)
		{
			RespondToConnectionMessage(sendingClient, (ConnectionMessage)object);
		}
	}
	
	/**
	 * Handles an incoming chat message.
	 * 
	 * @param sendingClient The sending client.
	 * @param chatMessage The chat message.
	 */
	private void RespondToChatMessage(SixDegreesServerClientConnection sendingClient, ChatMessage chatMessage)
	{
		Debug.DebugLog("Received a chat message from " + chatMessage.GetUsername());
		BroadcastObjectToAllClients(sendingClient, chatMessage);
	}
	
	/**
	 * Handles an incoming score message.
	 * 
	 * @param sendingClient The client that sent the message.
	 * @param scoreMessage The score message.
	 */
	private void RespondToScoreMessage(SixDegreesServerClientConnection sendingClient, ScoreMessage scoreMessage)
	{
		Debug.DebugLog("Received a score message from " + scoreMessage.GetUsername());
		BroadcastObjectToAllClients(sendingClient, scoreMessage);
		//m_previouslySentObjects.add(scoreMessage);
	}
	
	/**
	 * Handles an incoming question message.
	 * 
	 * @param sendingClient The client that sent the message.
	 * @param questionMessage The question message.
	 */
	private void RespondToQuestionMessage(SixDegreesServerClientConnection sendingClient, QuestionMessage questionMessage)
	{
		Debug.DebugLog("Received a question message from " + questionMessage.GetUsername());
	}
	
	/**
	 * Handles an incoming login message.
	 * 
	 * @param sendingClient The client that sent the message.
	 * @param loginMessage The login message
	 */
	private void RespondToLoginMessage(SixDegreesServerClientConnection sendingClient, LoginUserMessage loginMessage)
	{
		Debug.DebugLog("Received a login message from " + loginMessage.GetUsername());
		BroadcastObjectToAllClients(sendingClient, loginMessage);
		m_previouslySentObjects.add(loginMessage);

//		if(loginMessage.IsLoggingIn())
//		{
//			m_previouslySentObjects.add(loginMessage);
//		}
//		else
//		{
//			for(Object object : m_previouslySentObjects)
//			{
//				if(object instanceof LoginUserMessage)
//				{
//					if(((LoginUserMessage)object).GetUsername().equals(loginMessage.GetUsername()))
//					{
//						m_previouslySentObjects.remove(object);
//						break;
//					}
//				}
//			}
//		}
	}
	
	private void RespondToConnectionMessage(SixDegreesServerClientConnection sendingClient, ConnectionMessage connectionMessage)
	{
		Debug.DebugLog("Received a login message from " + connectionMessage.GetUsername());
		BroadcastObjectToAllClients(sendingClient, connectionMessage);
	}
}
